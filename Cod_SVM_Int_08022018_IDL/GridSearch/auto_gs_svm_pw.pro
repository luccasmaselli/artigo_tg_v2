FUNCTION AUTO_GS_SVM_PW, PATH_IMG, ptrROIs, Attributes, gamaPar, grauPol, gamaSig, penaltyPar, kFolds, trainPercent, sampling, Parameters_SVM
COMMON PkgPARAMETERS
COMMON PkgPOINTERS, PtrTRAINING

;parametros gerais
;ParamSTRUCT = {Penalty: 0.0, $
;               Kernel: 2, $
 ;              KernelParameters: [0.0,1.0,0.0], $
  ;             Strategy: 0}
;-------------------------------

Image = READ_TIFF(PATH_IMG)
Image = IMAGE_INSPECT(Image) ;Check image dimensions
Image = FLOAT(Image[Attributes,*,*]) ;Set the attribute

FOR i = 0, N_ELEMENTS(Image[*,0,0])-1 DO BEGIN
   IF MIN(Image[i,*,*]) LT 0 THEN $
      Image[i,*,*] = (Image[i,*,*] + ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] + ABS(MIN(Image[i,*,*]))) $
   ELSE $ 
      Image[i,*,*] = (Image[i,*,*] - ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] - ABS(MIN(Image[i,*,*])))
ENDFOR

IF Parameters_SVM.Kernel EQ 0 THEN BEGIN
loop = 1
parametro = 1
ENDIF
IF Parameters_SVM.Kernel EQ 1 THEN BEGIN
loop = N_ELEMENTS(grauPol) 
parametro = grauPol
ENDIF
IF Parameters_SVM.Kernel EQ 2 THEN BEGIN
loop = N_ELEMENTS(gamaPar)
parametro = gamaPar
ENDIF
IF Parameters_SVM.Kernel EQ 3 THEN BEGIN
loop = N_ELEMENTS(gamaSig)
parametro = gamaSig
ENDIF

;Matrix used to store the gridsearch information 
;gridMatrix = FLTARR(N_ELEMENTS(gamaPar), N_ELEMENTS(penaltyPar))
gridMatrix = FLTARR(loop, N_ELEMENTS(penaltyPar))


;fazer uma amosragem dentro de cada fold (para não pesar muito o svm)
ROIs = RESAMPLE_ROI(ptrROIs, sampling) ;amostragem aleatória de sampling% do total de pixels


;reformar... escolher regiões dentro dos ponteiros de cada classe
foldTV = GET_KFOLDS(Image, ROIs, kFolds, trainPercent) 


;FOR gam = 0, N_ELEMENTS(gamaPar)-1 DO BEGIN
FOR x = 0, loop-1 DO BEGIN
   FOR pen = 0, N_ELEMENTS(penaltyPar)-1 DO BEGIN

      ;parametros
      Parameters_SVM.Penalty = penaltyPar[pen]
      Parameters_SVM.KernelParameters[1] = parametro[x]         
         
      accFold = FLTARR(kFolds)
          
      FOR folds = 0, kFolds-1 DO BEGIN
       
         PntROIs = *foldTV[0,folds]
         PntTEST = *foldTV[1,folds]
         
         ;Define the classes used in the binary separation
         ;The returned value is used to build the training samples (as ROIs)
         
         ;stop
         BinDT = CLASS_SELECTOR(PntROIs,Parameters_SVM)
         
         ;Sorteio para equilibrar as classes de treino -- evitar problema de desbalanceamento em OAA
         ;BinDT = CLASS_EQUALIZATION(BinDT)
         ;BinDT = CLASS_EQUALIZATION_SAMPLED(BinDT,0.3)         

         Dims = size(Image,/dimension)
         NC = Dims[1]
         NL = Dims[2]

         testData = DATA_ACCURACY_SVM(PntTEST, Image)
         Discrim = FLTARR(N_ELEMENTS(BinDT[0,*]), N_ELEMENTS(testData.labelClass))

         FOR i = 0, N_ELEMENTS(BinDT[0,*])-1 DO BEGIN
            R1 = *BinDT[0,i]  &  R1 = R1.RoiLex
            R2 = *BinDT[1,i]  &  R2 = R2.RoiLex            

            ;Training data set building
            PtrTRAINING = BUILD_TRAINING_DATA(R1,R2,Image,Attributes,InImg)
            command = CREATE_COMMAND(Parameters_SVM)
            ;stop
            Discrim[i,*] = INTERFACE_LIBSVM_GRID_SEARCH(PtrTRAINING, Parameters_SVM,testData.dataClass, command)
            PTR_FREE, PtrTRAINING
         ENDFOR
         
        ; stop
         IF Parameters_SVM.Strategy EQ 0 THEN accFold[folds] = CHECK_OA_SVM_ACCURACY(Discrim,testData.labelClass)
         IF Parameters_SVM.Strategy EQ 1 THEN accFold[folds] = CHECK_OA_SVM_ACCURACY_OAO(Discrim,testData.labelClass,N_ELEMENTS(ptrROIs))   
         
         
         ;PTR_FREE, BinDT
         HEAP_GC   
      ENDFOR
            
      gridMatrix[x,pen] = MEAN(accFold)

   ENDFOR
   
ENDFOR

PTR_FREE, ROIs, foldTV
HEAP_GC ;coleta os lixos tambem...

;busca pela configuração de maior acuracia
FOR x = 0, loop-1 DO BEGIN
   FOR pen = 0, N_ELEMENTS(penaltyPar)-1 DO BEGIN
      IF gridMatrix[x,pen] EQ MAX(gridMatrix) THEN Return, [parametro[x],penaltyPar[pen]]
   ENDFOR
ENDFOR

stop
END




;######################################
FUNCTION RESAMPLE_ROI, ptrROIs, ratio

ROI = PTRARR(N_ELEMENTS(ptrROIs))
FOR i = 0, N_ELEMENTS(ptrROIs)-1 DO BEGIN
   Aux = *ptrROIs[i]
   
   rand = RANDOMU((systime(/seconds) mod 1000)+i, N_ELEMENTS(Aux.RoiLex))
   sort_rand = SORT(rand)
   sampled = Aux.RoiLex[sort_rand[0:floor(N_ELEMENTS(sort_rand)*ratio - 1)]]

   ROI[i] = PTR_NEW({RoiName: Aux.RoiName, RoiColor: Aux.RoiColor, RoiLex: sampled})
ENDFOR

Return, ROI
END




;######################################
 FUNCTION CHECK_OA_SVM_ACCURACY, Rules, labels

 itens = N_ELEMENTS(labels)
 MeasureAcc = 0L
 FOR i = 0L, itens-1 DO BEGIN
     Index = WHERE(Rules[*,i] EQ MAX(Rules[*,i]))
     IF Index[0] EQ labels[i] THEN MeasureAcc++
ENDFOR 
 
Return, FLOAT(MeasureAcc)/itens
END
;###########################################################

 FUNCTION CHECK_OA_SVM_ACCURACY_OAO, Rules, labels, ClaNum

ND = N_ELEMENTS(Rules[*,0])
Referee = INTARR(ND)
Wins = INTARR(ClaNum)
itens = N_ELEMENTS(labels)
MeasureAcc = 0L
 
 
FOR i = 0L, itens-1 DO BEGIN

   FOR k = 0, ND-1 DO BEGIN
         IF(Rules[k,i] GE 0) THEN Referee[k] = +1 ELSE Referee[k] = -1 
   ENDFOR
      
   pos = 0
   Wins[*] = 0
  
   FOR c1 = 0, (ClaNum-2) DO BEGIN
     FOR c2 = (c1 + 1), (ClaNum-1) DO BEGIN
         IF (Referee[pos] EQ +1) THEN Wins[c1]++ ELSE Wins[c2]++ 
         pos++
     ENDFOR
   ENDFOR
      
   Index = WHERE(Wins EQ Max(Wins))
   IF Index[0] EQ labels[i] THEN MeasureAcc++
   
ENDFOR 
 
Return, FLOAT(MeasureAcc)/itens
END


;######################################
FUNCTION CLASS_EQUALIZATION, BinGroups

BinEqual = PTRARR(2,N_ELEMENTS(BinGroups[0,*]))
FOR i = 0, N_ELEMENTS(BinGroups[0,*])-1 DO BEGIN
   yPlus = *BinGroups[0,i]
   yMinus = *BinGroups[1,i]
   
   elCut = N_ELEMENTS(yPlus.RoiLex)
   elems = N_ELEMENTS(yMinus.RoiLex) 
   rand = SORT(RANDOMU(systime(/seconds)*100000 MOD 1000, elems))
   
   Minus = {RoiName: yMinus.RoiName, RoiColor: yMinus.RoiColor, RoiLex: yMinus.RoiLex[rand[0:elCut-1]]}
      
   BinEqual[0,i] = PTR_NEW(yPlus)
   BinEqual[1,i] = PTR_NEW(Minus)
ENDFOR

;PTR_FREE, BinGroups
HEAP_GC

Return, BinEqual
END


;######################################
FUNCTION CLASS_EQUALIZATION_SAMPLED, BinGroups, sampPercent

BinEqual = PTRARR(2,N_ELEMENTS(BinGroups[0,*]))
FOR i = 0, N_ELEMENTS(BinGroups[0,*])-1 DO BEGIN
   yPlus = *BinGroups[0,i]
   yMinus = *BinGroups[1,i]
   
   elP = N_ELEMENTS(yPlus.RoiLex)
   elM = N_ELEMENTS(yMinus.RoiLex) 
   
   elCut = Round(elP*sampPercent)
   
   randPlus = SORT(RANDOMU(systime(/seconds)*100000 MOD 500, elP))
   randMinus = SORT(RANDOMU(systime(/seconds)*100000 MOD 1000, elM))   
   
   Plus = {RoiName: yPlus.RoiName, RoiColor: yPlus.RoiColor, RoiLex: yPlus.RoiLex[randPlus[0:elCut-1]]}
   Minus = {RoiName: yMinus.RoiName, RoiColor: yMinus.RoiColor, RoiLex: yMinus.RoiLex[randMinus[0:elCut-1]]}
      
   BinEqual[0,i] = PTR_NEW(Plus)
   BinEqual[1,i] = PTR_NEW(Minus)
ENDFOR

;PTR_FREE, BinGroups
HEAP_GC

Return, BinEqual
END




;#######################################################
FUNCTION  GET_KFOLDS, Image, PtrROIs, kFolds, trainPercent

foldTV = PTRARR(2,kFolds)
foldClass = PTRARR(2,N_ELEMENTS(PtrROIs),kFolds)

FOR class = 0L, N_ELEMENTS(PtrROIs)-1 DO BEGIN
   
   TRegs = GET_INDIVIDUAL_SAMPLES(Image, PtrROIs[class]) ;Gets the trainign regions
   TRegs = *TRegs[0]
   rands = RANDOMN((SYSTIME(/SECONDS)*1000 MOD 10000)+class, kFolds, N_ELEMENTS(TRegs))
   trainPos = FLOOR(N_ELEMENTS(TRegs)*(trainPercent/100.0))
   FOR fold = 0L, kFolds-1 DO BEGIN
      ord = SORT(rands[fold,*])
      foldClass[0,class,fold] = PTR_NEW(PACK_ROI(TRegs[ord[0 : trainPos-1]], *PtrROIs[class]))
      foldClass[1,class,fold] = PTR_NEW(PACK_ROI(TRegs[ord[trainPos : N_ELEMENTS(TRegs)-1]], *PtrROIs[class]))
   ENDFOR
   
ENDFOR

;packing
FOR i = 0, kFolds-1 DO BEGIN
   foldTV[0,i] = PTR_NEW( foldClass[0,*,i] )
   foldTV[1,i] = PTR_NEW( foldClass[1,*,i] )
ENDFOR

Return, foldTV
END



;#######################################################
FUNCTION GET_INDIVIDUAL_SAMPLES, Image, PtrROIs

ImageSamples = DBLARR(N_ELEMENTS(Image[0,*,0]), N_ELEMENTS(Image[0,0,*]))
ImageSamples[*] = Image[[0],*,*]

IndvSamples = PTRARR(N_ELEMENTS(PtrROIs))
FOR i = 0, N_ELEMENTS(PtrROIs)-1 DO BEGIN
   Roi = *PtrROIs[i]
   ImageSamples[*] = 0

   FOR j = 0L, N_ELEMENTS(Roi.RoiLex)-1 DO $
      ImageSamples[Roi.RoiLex[j]] = 1
    
   Seps = SEPARATE_SAMPLES(ImageSamples)
   IndvSamples[i] = PTR_NEW(Seps)
ENDFOR

Return, IndvSamples
END



;#######################################################
FUNCTION SEPARATE_SAMPLES, ImageSample
COMMON PkgIndex, IdReg, Idx
COMMON PkgDims, NC, NL

Dim = SIZE(ImageSample,/DIMENSIONS)
NC = Dim[0] & NL = Dim[1]
IdReg = 0

Segm = ImageSample & Segm[*] = -9999
Res = ImageSample

FOR i = 0L, N_ELEMENTS(Res)-1 DO $
   IF ImageSample[i] EQ 1 THEN Res[i] = 0 ELSE Res[i] = -9999
   
   Segm = CONNECTED_PIXELS(Res)

Samples = [PTR_NEW()]
FOR i = 0, MAX(Segm) DO BEGIN
   Ind = WHERE(Segm EQ i)
   IF Ind[0] NE -1 THEN Samples = [Samples , PTR_NEW(Ind)]
ENDFOR

IF N_ELEMENTS(Samples) NE 1 THEN Return, Samples[1:N_ELEMENTS(Samples)-1] $
ELSE Return, Samples
END


;#######################################################
FUNCTION CONNECTED_PIXELS, Segm
COMMON PkgSeeds, Seeds
COMMON PkgIndex, IdReg, Idx
COMMON PkgDims, NC, NL
COMMON PkgSegm, Img

Seeds = WHERE(Segm EQ 0) ;Sementes para crescer a regiao
Seeds = [Seeds , -1]     ; o -1 identifica fim da fila

Img = Segm

WHILE Seeds[0] NE -1 DO BEGIN
   pos = Seeds[0]   
   Segm = GROW([pos])
ENDWHILE

Return, Segm
END