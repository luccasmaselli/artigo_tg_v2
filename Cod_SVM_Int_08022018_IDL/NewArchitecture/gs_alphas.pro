FUNCTION GS_ALPHAS, PATH_IMG, PntROIs, Attributes, gamaPar, grauPol, gamaSig, penaltyPar, kFolds, trainPercent, sampling, Parameters_SVM, alpha1, alpha2, alpha3, alpha4
COMMON PkgPARAMETERS
COMMON PkgPOINTERS, PtrTRAINING

;parametros gerais
;ParamSTRUCT = {Penalty: 0.0, $
;               Kernel: 2, $
 ;              KernelParameters: [0.0,1.0,0.0], $
  ;             Strategy: 0}
;-------------------------------

REPORT_PATH = '/home/luccas/Desktop/OA_GridSearch_Landsat_OAO_v3.txt'


Image = READ_TIFF(PATH_IMG)
Image = IMAGE_INSPECT(Image) ;Check image dimensions
Image = FLOAT(Image[Attributes,*,*]) ;Set the attribute

FOR i = 0, N_ELEMENTS(Image[*,0,0])-1 DO BEGIN
   IF MIN(Image[i,*,*]) LT 0 THEN $
      Image[i,*,*] = (Image[i,*,*] + ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] + ABS(MIN(Image[i,*,*]))) $
   ELSE $ 
      Image[i,*,*] = (Image[i,*,*] - ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] - ABS(MIN(Image[i,*,*])))
ENDFOR


BinDT = CLASS_SELECTOR(PntROIs,Parameters_SVM)

Dims = size(Image,/dimension)
NC = Dims[1]
NL = Dims[2]

testData = DATA_ACCURACY_SVM(PntROIs, Image)
DiImageLin = FLTARR(N_ELEMENTS(BinDT[0,*]), N_ELEMENTS(testData.labelClass))
DiImagePol = FLTARR(N_ELEMENTS(BinDT[0,*]), N_ELEMENTS(testData.labelClass))
DiImageRBF = FLTARR(N_ELEMENTS(BinDT[0,*]), N_ELEMENTS(testData.labelClass))
DiImageSig = FLTARR(N_ELEMENTS(BinDT[0,*]), N_ELEMENTS(testData.labelClass))

FOR i = 0, N_ELEMENTS(BinDT[0,*])-1 DO BEGIN
   R1 = *BinDT[0,i]  &  R1 = R1.RoiLex
   R2 = *BinDT[1,i]  &  R2 = R2.RoiLex            

   ;Training data set building
   PtrTRAINING = BUILD_TRAINING_DATA(R1,R2,Image,Attributes,InImg)
            
   command = NEW_CREATE_COMMAND(Parameters_SVM, 0)
   DiImageLin[i,*] = INTERFACE_LIBSVM_GRID_SEARCH(PtrTRAINING, Parameters_SVM,testData.dataClass, command)
   
   command = NEW_CREATE_COMMAND(Parameters_SVM, 1)
   DiIMagePol[i,*] = INTERFACE_LIBSVM_GRID_SEARCH(PtrTRAINING, Parameters_SVM,testData.dataClass, command)
   
   command = NEW_CREATE_COMMAND(Parameters_SVM, 2)
   DiImageRBF[i,*] = INTERFACE_LIBSVM_GRID_SEARCH(PtrTRAINING, Parameters_SVM,testData.dataClass, command)
   
   command = NEW_CREATE_COMMAND(Parameters_SVM, 3)
   DiImageSig[i,*] = INTERFACE_LIBSVM_GRID_SEARCH(PtrTRAINING, Parameters_SVM,testData.dataClass, command)
   PTR_FREE, PtrTRAINING
ENDFOR
      
HEAP_GC 

accAlphaLC = FLTARR(N_ELEMENTS(alpha1), N_ELEMENTS(alpha2), N_ELEMENTS(alpha3), N_ELEMENTS(alpha4)) - 9999.9   
FOR a = 0, N_ELEMENTS(alpha1)-1 DO BEGIN
   FOR b = 0, N_ELEMENTS(alpha2)-1 DO BEGIN
      FOR c = 0, N_ELEMENTS(alpha3)-1 DO BEGIN
         FOR d = 0, N_ELEMENTS(alpha4)-1 DO BEGIN
                                     
            IF (alpha1[a]+alpha2[b]+alpha3[c]+alpha4[d]) EQ 1.0 THEN BEGIN
               Discrim = alpha1[a]*DiImageLin + alpha2[b]*DiImagePol + alpha3[c]*DiImageRBF + alpha4[d]*DiImageSig
                  
               IF Parameters_SVM.Strategy EQ 0 THEN accuracy = CHECK_OA_SVM_ACCURACY(Discrim,testData.labelClass)
               IF Parameters_SVM.Strategy EQ 1 THEN accuracy = CHECK_OA_SVM_ACCURACY_OAO(Discrim,testData.labelClass,N_ELEMENTS(PntROIs))   
                         
               accAlphaLC[a,b,c,d] = accuracy
               
               OpenW, Arq, REPORT_PATH, /APPEND, /GET_LUN
               PrintF, Arq, accuracy, ' ; '
               Close, Arq
               Free_lun, Arq
               
            ENDIF
                                    
         ENDFOR
      ENDFOR
   ENDFOR
ENDFOR 


bestAcc = -1.0
confAlpha = FLTARR(4)
FOR a = 0, N_ELEMENTS(alpha1)-1 DO BEGIN
   FOR b = 0, N_ELEMENTS(alpha2)-1 DO BEGIN
      FOR c = 0, N_ELEMENTS(alpha3)-1 DO BEGIN
         FOR d = 0, N_ELEMENTS(alpha4)-1 DO BEGIN
         
            IF accAlphaLC[a,b,c,d] GT bestAcc THEN BEGIN
               bestAcc = accAlphaLC[a,b,c,d]
               confAlpha = [alpha1[a],alpha2[b],alpha3[c],alpha4[d]]
            ENDIF
         
         ENDFOR
      ENDFOR
   ENDFOR
ENDFOR                         
        
HEAP_GC ;coleta os lixos tambem...

;print, bestAcc
;print, confAlpha


return, confAlpha
END