FUNCTION MAPPER, Image, PtrROIs_LS_CLASS, TEUs_STATS
  
   dims = SIZE(Image, /dimension)
   NB = dims[0]
   NCol = dims[1]
   NLin = dims[2]
   TEU_IMG = BYTARR(NCol,NLin)-1
   ;alpha = 0.05 ; confiabilidade dos TEUs
   window_stats = DBLARR(5,4) ;4 colunas (classes); 4 linhas (Estatísticas)
   
   posicoes = DBLARR(NCol*NLin)
   FOR k=0L, N_ELEMENTS(posicoes)-1 DO posicoes[k]=k ;vetor onde cada posição é preenchida com o respectivo índice
   
   dims_ls = [dims[1], dims[2]]
  ; BUILD IMAGE - ls
  Index_Image = BUILD_IMAGE(Image, dims_ls, posicoes, PtrROIs_LS_CLASS) ; retorna imagens com índices para facilitar calculo de métricas
  Index_Image_group = GROUP_CLASSES_BR(Index_Image)
    
  conv_window = INTARR(13,13) ; janela convolutiva 13x13
  TEU_matrix = BYTARR(13,13)
  dims_win = [13,13]
  
    FOR i = 6L, NLin-7 DO BEGIN ; janela convolutiva 13x13
    FOR j = 6L, NCol-7 DO BEGIN ; janela convolutiva 13x13
      conv_window = Index_Image_group[j-6:j+6,i-6:i+6]
      proporcao = CALC_PROPORTIONS(conv_window) ; 1 linha (estatistica) 4 colunas (classes)
      metricas = CALC_METRICAS(conv_window, dims_win[0], dims_win[1]) ; 3 linhas (estatísticas) 4 colunas (classes)
      
      ; calculado a estatística do janela, se inicia a comparação e atribuição de TEUs
      ; chave de atribuição: proporção entre classes -> coef var -> dens manchas -> dens bordas
      
      window_stats[*,0] = proporcao
      window_stats[*,1:3] = metricas
      
      Att_TEU = COMPARADOR(window_stats, TEUs_STATS, alpha)
      
      ;FOR k=0L, 168 DO TEU_matrix[k] = Att_TEU
      
      ;TEU_IMG[j,i] = TEU_matrix
      TEU_IMG[j,i] = Att_TEU
      print, i, j, Att_TEU
      ;j = j+13
    ENDFOR
    ;print, i, j
    ;i = i+13
  ENDFOR
;  FOR i = 6L, NLin-7 DO BEGIN ; janela convolutiva 13x13
;    FOR j = 6L, NCol-7 DO BEGIN ; janela convolutiva 13x13
;      conv_window = Index_Image[j-6:j+6,i-6:i+6]
;      proporcao = CALC_PROPORTIONS_BR(conv_window) ; 1 linha (estatistica) 4 colunas (classes)
;      metricas = CALC_METRICAS_BR(conv_window) ; 3 linhas (estatísticas) 4 colunas (classes)
;      
;      ; calculado a estatística do janela, se inicia a comparação e atribuição de TEUs
;      ; chave de atribuição: proporção entre classes -> coef var -> dens manchas -> dens bordas
;      
;      window_stats[*,0] = proporcao
;      window_stats[*,1:3] = metricas
;      
;      Att_TEU = COMPARADOR(window_stats, TEUs_STATS, alpha)
;      
;      TEU_IMG[j,i] = Att_TEU
;      print, i, j, Att_TEU
;    ENDFOR
;    print, i, j
;  ENDFOR
  
Return, TEU_IMG
  
END   