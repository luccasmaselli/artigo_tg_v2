PRO STATS_REPORT, REPORT_PATH, ContPrc, estats
    
   ;Formated confusion matrix
   ;StringCM = CONFUSION_MATRIX_FORMATER(ConfusionMatrix, PntROI, TestROI)
   
   OpenW, Arq, REPORT_PATH, /APPEND, /GET_LUN
   
   
   PrintF, Arq, '                 TEL_CERAMICA  VEGETACAO  AGUA  SOLO_EXPOSTO PISCINA ASFALTO SOMBRA  TEL_CONZA TEL_CLARO PASTO'
   PrintF, Arq, '#pix TEU MF I:  ', ContPrc(*,0)
   PrintF, Arq, '#pix TEU MF II: ', ContPrc(*,1)
   PrintF, Arq, '#pix TEU AP I:  ', ContPrc(*,2)
   PrintF, Arq, '#pix TEU AP II: ', ContPrc(*,3)
   PrintF, Arq, '#pix TEU MP:    ', ContPrc(*,4)
   PrintF, Arq, '#pix TEU BP I:  ', ContPrc(*,5)
   PrintF, Arq, '#pix TEU BP II: ', ContPrc(*,6)
   PrintF, Arq, '#pix TEU COMER: ', ContPrc(*,7)
   PrintF, Arq, '#pix TEU INDUS: ', ContPrc(*,8)
   PrintF, Arq, '#pix TEU AR_VER:', ContPrc(*,9)
   PrintF, Arq, '#pix TEU AGRIC: ', ContPrc(*,10)
   PrintF, Arq, '#pix TEU ZEA:   ', ContPrc(*,11)
   PrintF, Arq, ''
   PrintF, Arq, 'Coeficiente Variação'
   PrintF, Arq, 'TEU MF I:  ', TRANSPOSE(estats[0,*,0])
   PrintF, Arq, 'TEU MF II: ', TRANSPOSE(estats[1,*,0])
   PrintF, Arq, 'TEU AP I:  ', TRANSPOSE(estats[2,*,0])
   PrintF, Arq, 'TEU AP II: ', TRANSPOSE(estats[3,*,0])
   PrintF, Arq, 'TEU MP:    ', TRANSPOSE(estats[4,*,0])
   PrintF, Arq, 'TEU BP I:  ', TRANSPOSE(estats[5,*,0])
   PrintF, Arq, 'TEU BP II: ', TRANSPOSE(estats[6,*,0])
   PrintF, Arq, 'TEU COMER: ', TRANSPOSE(estats[7,*,0])
   PrintF, Arq, 'TEU INDUS: ', TRANSPOSE(estats[8,*,0])
   PrintF, Arq, 'TEU AR_VER:', TRANSPOSE(estats[9,*,0])
   PrintF, Arq, 'TEU AGRIC: ', TRANSPOSE(estats[10,*,0])
   PrintF, Arq, 'TEU ZEA:   ', TRANSPOSE(estats[11,*,0])
   PrintF, Arq, ''
   PrintF, Arq, 'Densidade de Manchas'
   PrintF, Arq, 'TEU MF I:  ', TRANSPOSE(estats[0,*,1])
   PrintF, Arq, 'TEU MF II: ', TRANSPOSE(estats[1,*,1])
   PrintF, Arq, 'TEU AP I:  ', TRANSPOSE(estats[2,*,1])
   PrintF, Arq, 'TEU AP II: ', TRANSPOSE(estats[3,*,1])
   PrintF, Arq, 'TEU MP:    ', TRANSPOSE(estats[4,*,1])
   PrintF, Arq, 'TEU BP I:  ', TRANSPOSE(estats[5,*,1])
   PrintF, Arq, 'TEU BP II: ', TRANSPOSE(estats[6,*,1])
   PrintF, Arq, 'TEU COMER: ', TRANSPOSE(estats[7,*,1])
   PrintF, Arq, 'TEU INDUS: ', TRANSPOSE(estats[8,*,1])
   PrintF, Arq, 'TEU AR_VER:', TRANSPOSE(estats[9,*,1])
   PrintF, Arq, 'TEU AGRIC: ', TRANSPOSE(estats[10,*,1])
   PrintF, Arq, 'TEU ZEA:   ', TRANSPOSE(estats[11,*,1])
   PrintF, Arq, ''
   PrintF, Arq, 'Densidade de Bordas'
   PrintF, Arq, 'TEU MF I:  ', TRANSPOSE(estats[0,*,2])
   PrintF, Arq, 'TEU MF II: ', TRANSPOSE(estats[1,*,2])
   PrintF, Arq, 'TEU AP I:  ', TRANSPOSE(estats[2,*,2])
   PrintF, Arq, 'TEU AP II: ', TRANSPOSE(estats[3,*,2])
   PrintF, Arq, 'TEU MP:    ', TRANSPOSE(estats[4,*,2])
   PrintF, Arq, 'TEU BP I:  ', TRANSPOSE(estats[5,*,2])
   PrintF, Arq, 'TEU BP II: ', TRANSPOSE(estats[6,*,2])
   PrintF, Arq, 'TEU COMER: ', TRANSPOSE(estats[7,*,2])
   PrintF, Arq, 'TEU INDUS: ', TRANSPOSE(estats[8,*,2])
   PrintF, Arq, 'TEU AR_VER:', TRANSPOSE(estats[9,*,2])
   PrintF, Arq, 'TEU AGRIC: ', TRANSPOSE(estats[10,*,2])
   PrintF, Arq, 'TEU ZEA:   ', TRANSPOSE(estats[11,*,2])
   
   Close, Arq
   Free_lun, Arq
END