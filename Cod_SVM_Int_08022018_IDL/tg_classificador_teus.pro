@bocalib.pro
@connected_components_edge.pro
@calc_metricas.pro
@get_edges.pro
@calc_proportions.pro
@tg_estatisticas.pro
@mapper.pro
@build_image.pro
@build_image_br.pro
@group_classes.pro
@group_classes_br.pro

PRO TG_CLASSIFICADOR_TEUS

   ; CAMINHO IMAGEM CLASSIFICADA
   PATH_CLASS_IMG_BAIXA_RES = "C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO_TG\2 - RESULTADOS\CLASS_PRIM_SENT\BEST_RESULTS\ls07_1999_ClassPrim_           2_     0.250000_    1000_           1.tif"
   ;PATH_CLASS_IMG_BAIXA_RES = '/home/luccas/Desktop/tentativa recomecar/CLASSIFICACOES/LS2017/Classificacoes+roisteste/Lin_1000_OAA_Class_Support Vector Machine__Thu Oct 11 13:57:02 2018.tif'
   
   
   ; CAMINHO PARA ROIs DE TREINO
   PATH_LS_CLASS_ROI = "C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO_TG\1 - INSUMOS\ROIs\Sentinel\ROI_Sent_PRIM_treino.txt"
   ;PATH_LS_CLASS_ROI = '/home/luccas/Desktop/tentativa recomecar/ROI_POINTS_LS08.txt'

   ; CAMINHO PARA RESULTADO
   PATH_RESULT = "C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO_TG\2 - RESULTADOS\2 - STATS_IMG\SENTINEL\" 

   ; Lê a imagem
   Image_BR = READ_TIFF(PATH_CLASS_IMG_BAIXA_RES) 

   ; Lê os ROIs
   PtrROIs_LS_CLASS = ASCII_READ_ROI(PATH_LS_CLASS_ROI)
   
   ; Define o tamanho da janela considera no contexto das estatísticas (h X h)
   h = [35, 37, 39, 41]
   FOR i=0L, N_ELEMENTS(h) DO BEGIN
   hh=h[i]
   ; Função que monta a imagem de estatísticas
   IMG_STATS = BUILD_STATS_IMAGE(Image_BR, PtrROIs_LS_CLASS, hh)
   
   ; Exporta o resultado para formato .tif
   write_tiff, PATH_RESULT + 'h' + string(hh) + '_img_estatisticas_conv_win_ls07_1999.tif', IMG_STATS, /float
   ENDFOR
   ; Função que classifica de acordo com os TEUs
;   TEU_IMG = SVM_TEU(IMG_STATS)
   stop
END