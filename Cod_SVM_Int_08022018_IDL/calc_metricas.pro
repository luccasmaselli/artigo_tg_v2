FUNCTION CALC_METRICAS, image, NCol, NLin, NClass
   
  teu_comp_conect = LONARR(NCol, NLin)
  
  ;NClass = MAX(image[uniq(image, sort(image))]) ;para criar vetor de todas as classes
  Estats = DBLARR(NClass,3) ;col (10 colunas => classes) & lin (12 linhas => 12 teus)
  
  ;cria vetores de dimensão igual ao número de classes
  sum_vec = FLTARR(NClass)
  count = FLTARR(NClass)
  media = FLTARR(NClass)
  dividendo_vec = FLTARR(NClass)

   comp_conect_img = CONNECTED_COMPONENTS(image) ;constrói a imagem de componentes conectados
   
   sort_vec = comp_conect_img[UNIQ(comp_conect_img, SORT(comp_conect_img))] ;ordena os índices de componentes conectados
   
   FOR j=0L, N_ELEMENTS(sort_vec)-1 DO BEGIN ;calculo das variáveis que compõem a média
    pos_id = WHERE(comp_conect_img eq sort_vec[j]) ;acha todas as posições das componentes conectadas 
    ;sum_vec[image(pos_id(0))] = sum_vec[image(pos_id(0))] + N_ELEMENTS(pos_id) ;realiza a contagem de pixels de cada classe
    sum_vec[image(pos_id(0))] += N_ELEMENTS(pos_id) ;realiza a contagem de pixels de cada classe
    count[image(pos_id(0))] = count[image(pos_id(0))] + 1 ;conta o numero de manchas de cada classe
   ENDFOR
   
   INDEX = WHERE(count EQ 0, contador)
   IF contador NE 0 THEN count(INDEX) = 1 ;evita divisão 0/0
   
   media = sum_vec/count ;calcula o tamanho médio das manchas
   
   FOR k=0L, N_ELEMENTS(sort_vec)-1 DO BEGIN ;calculo das variáveis que compõem o desvio padrão
    pos_id = WHERE(comp_conect_img eq sort_vec[k]) ;acha todas as posições das componentes conectadas
    dividendo_vec[image(pos_id(0))] += ((N_ELEMENTS(pos_id) - media(image(pos_id(0))))^2) ;calcula o vetor com o dividendo do desv pad
   ENDFOR
   
   desv_pad = SQRT(dividendo_vec/count)
   
   INDEX = WHERE(media EQ 0, contador) 
   IF contador NE 0 THEN media(INDEX) = 1 ;evita divisão 0/0
   
   Coef_var = desv_pad/media
   IF MAX(Coef_var)-MIN(Coef_var) EQ 0 THEN DIV = 1 ELSE DIV = MAX(Coef_var)-MIN(Coef_var) 
    Coef_var = (Coef_var-MIN(Coef_var))/DIV
   
   
   Dens_manchas = count/DOUBLE(NCol*NLin) ;área da imagem
   IF MAX(Dens_manchas)-MIN(Dens_manchas) EQ 0 THEN DIV = 1 ELSE DIV = MAX(Dens_manchas)-MIN(Dens_manchas)
    Dens_manchas = (Dens_manchas-MIN(Dens_manchas))/DIV
    
   Edge_lenghts = CONNECTED_COMPONENTS_EDGE(image, NCLass)
   
   Dens_bordas = Edge_lenghts/DOUBLE(NCol*NLin)
   IF MAX(Dens_bordas)-MIN(Dens_bordas) EQ 0 THEN DIV = 1 ELSE DIV = MAX(Dens_bordas)-MIN(Dens_bordas)
    Dens_bordas = (Dens_bordas-MIN(Dens_bordas))/DIV   
;  Estats(*,0) = Coef_var
;  Estats(*,1) = Dens_manchas
;  Estats(*,2) = Dens_bordas

  Return, [Coef_var, Dens_manchas, Dens_bordas]
END