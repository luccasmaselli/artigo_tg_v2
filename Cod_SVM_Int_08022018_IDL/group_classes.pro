FUNCTION GROUP_CLASSES, id_image
  
  dims = SIZE(id_image, /dimension)
  NCol = dims[0]
  NLin = dims[1]
  
  id_img_group = INTARR(NCol,NLin)
  
  INDEX = WHERE(id_image EQ 2 OR id_image EQ 4, count)
  IF count NE 0 THEN id_img_group(INDEX) = 0 ;AGUA
  INDEX = WHERE(id_image EQ 1 OR id_image EQ 9, count)
  IF count NE 0 THEN id_img_group(INDEX) = 1 ;VEGETACAO
  INDEX = WHERE(id_image EQ 3 OR id_image EQ 5, count)
  IF count NE 0 THEN id_img_group(INDEX) = 2 ;ARRUAMENTO
  INDEX = WHERE(id_image EQ 0 OR id_image EQ 6, count)
  IF count NE 0 THEN id_img_group(INDEX) = 3 ;EDIFICACOES CERAMICA
  INDEX = WHERE(id_image EQ 7 OR id_image EQ 8, count)
  IF count NE 0 THEN id_img_group(INDEX) = 4 ;EDIFICACOES CLAROS  
  Return, id_img_group
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 0 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL_CERAMICA
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 255 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 1 ;VEGETACAO
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 0 and image[2,*,*] eq 255, count)
;      IF count NE 0 THEN image_ids(INDEX) = 0 ;AGUA
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 255 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 2 ;SOLO EXPOSTO
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 255 and image[2,*,*] eq 255, count) 
;      IF count NE 0 THEN image_ids(INDEX) = 0 ;PISCINA
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 0 and image[2,*,*] eq 255, count)
;      IF count NE 0 THEN image_ids(INDEX) = 2 ;ASFALTO
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 255 and image[2,*,*] eq 255, count) 
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;SOMBRA
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 0 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL CINZA
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 165 and image[2,*,*] eq 0, count) 
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL CLARO
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 100 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 1 ;PASTO
END