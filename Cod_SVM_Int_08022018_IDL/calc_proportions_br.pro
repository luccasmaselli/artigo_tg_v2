FUNCTION CALC_PROPORTIONS_BR, Image
    
   dims = size(Image, /dimension)
   ;NB = dims[0]
   NCol = dims[0]
   NLin = dims[1]
   ;NClass = N_ELEMENTS(PtrROIs)
   
   ;CONTAGEM DE PIXEL POR CLASSES EM CADA ROI
   ; 0 = AGUA; 1 = VEGETACAO; 2 = ARRUAMENTO e 3 = EDIFICAÇÕES
   
   Cont = LONARR(4); cada coluna é uma classe (4 classes) dentro da janela convolutiva
   FOR i = 0L, NCol-1 DO BEGIN
    FOR j = 0L, NLin-1 DO BEGIN
      IF Image(i,j) EQ 0 THEN Cont[3]++ ;TEL_CERAMICA
      IF Image(i,j) EQ 1 THEN Cont[1]++ ;VEGETACAO
      IF Image(i,j) EQ 2 THEN Cont[0]++ ;AGUA
      IF Image(i,j) EQ 3 THEN Cont[2]++ ;SOLO EXPOSTO
      IF Image(i,j) EQ 4 THEN Cont[2]++ ;ASFALTO
      IF Image(i,j) EQ 5 THEN Cont[3]++ ;TEL_CINZA
      IF Image(i,j) EQ 6 THEN Cont[1]++ ;PASTO
    ENDFOR
   ENDFOR
       
   Return, Cont/TOTAL(Cont)
END