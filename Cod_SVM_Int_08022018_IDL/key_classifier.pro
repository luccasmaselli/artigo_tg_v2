FUNCTION KEY_CLASSIFIER, win_stats, teus_stats, alpha

   dims = SIZE(teus_stats, /dimension)
   N_Class = dims[0] ; colunas (Classes)
   N_TEUs = dims[1] ; linhas (TEUs)
   N_metrics = dims[2] ; bandas (estatísiticas)
   
   votos = INTARR(N_TEUs+1) ; soma 1 para ser a classe 'Unclassified' caso nao se encaixar em nenhuma
   
   ; Primeiro desenpate: proporção com 10%
   FOR i = 0L, N_metrics-1 DO BEGIN
    FOR j = 0L, N_TEUs-1 DO BEGIN
      Aux = 0
      FOR k = 0L, N_Class-1 DO BEGIN      
          IF teus_stats(k,j,i) GT (win_stats(k,i) - win_stats(k,i)*alpha) AND $
                teus_stats(k,j,i) LT (win_stats(k,i) + win_stats(k,i)*alpha) THEN Aux = Aux + 1
     ENDFOR
        IF Aux EQ N_Class THEN votos(j) = votos(j) + 1
        
    ENDFOR ; aqui acaba de verificar as proporções no 1º looping
     IF MAX(votos) EQ 0 THEN votos(N_ELEMENTS(votos)-1) = votos(N_ELEMENTS(votos)-1) + 1 ; voto vai para 'Unclassified)
     IF N_ELEMENTS(WHERE(votos EQ MAX(votos))) EQ 1 THEN BREAK ELSE CONTINUE
   ENDFOR

   Return, WHERE(votos EQ MAX(votos))
   
   END