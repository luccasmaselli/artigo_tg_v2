FUNCTION TG_Estatisticas, Image, PtrROIs, PtrROIs_IK_CLASS
    
  dims = size(Image, /dimension)
  NB = dims[0]
  NCol = dims[1]
  NLin = dims[2]
  
  Estatisticas = DBLARR(5,N_ELEMENTS(PtrROIs),4) ;4 colunas (classes); 12 linhas (TEUs) 4 bandas (Estatísticas)
  ; ARRUMAR AQUI PARA DIMINUIR NUM. DE CLASSES
  
  FOR i=0L, N_ELEMENTS(PtrROIs)-1 DO BEGIN
  
  AuxTEU = *PtrROIs[i]
  posicoes = AuxTEU.RoiLex
  dims_teu = [UINT(SQRT(N_ELEMENTS(AuxTEU.RoiLex))),UINT(SQRT(N_ELEMENTS(AuxTEU.RoiLex)))]
  Id_Image = BUILD_IMAGE(Image, dims_teu, posicoes, PtrROIs_IK_CLASS) ;monta a imagem dentro do TEU por índices
  
  
  Id_img_group = GROUP_CLASSES(Id_Image) ;agrupa os índices (mudar essa função em caso de troca de agrupamento de classes)
  ;Estatisticas = Porcentagem; Coef. Variação; Dens. Manchas; Dens. Bordas
  
  ContPrc = CALC_PROPORTIONS(Id_img_group) ; calcula a porcentagem de cada classe dentro dos TEUs [10 CLASSES E 12 TEUS]
  estats = CALC_METRICAS(Id_img_group, dims_teu[0], dims_teu[1])
  
  Estatisticas[*,i,0] = ContPrc
  Estatisticas[*,i,1:3] = estats
  
  ENDFOR
  
  Return, Estatisticas
END