FUNCTION CONNECTED_COMPONENTS, image
   
   Dim = SIZE(image, /DIMENSION)
   NC = Dim[0]
   NL = Dim[1]
       
   freePos = INTARR(NC,NL) - 1 ;initializing all elements as -1 to denote 'free positions'
   Connected = LONARR(NC,NL) ;inicia a matriz que recebe a imagem de componentes conectadas
   
   newIdReg = 1L
   REPEAT BEGIN
      candidates = WHERE(freePos EQ -1) ;acha a posição dos candidatos (posições preenchidas por -1)
      rand = (LONG(RANDOMU(systime(/seconds))*10000000L)) MOD N_ELEMENTS(candidates) ;sorteia uma posição 
      seedPos = candidates[rand] ;por fim, seleciona uma posição livre (-1) aleatória
      
      targetID = image[seedPos] ;seleciona o valor da imagem (classe) correspondente a posição livre aleatória
      list = [seedPos , -999L] ;guarda a posição livre aleatória e um numero negativo muito pequeno (-999)
      
      WHILE (N_ELEMENTS(list) GT 1) DO BEGIN
         choose = list[0] ;pega a posição livre aleatória
         Connected[choose] = newIdReg ;a matriz de componentes conectados, na posição aleatória recebe seu indice
         freePos[choose] = 255 ;matriz de posições livre recebe 255 (indica que nao está livre)
         list = list[1:*] ;extrai o primeiro termo do vetor list
      
         neighs = GET_NEIGHS_CROSS(image, freePos, targetID, choose) ;função que entra a imagem, matriz de posições livres, classe em questão e posição livre aleatória
         ;retorna as posições que estão livre e possuem a mesma classe do alvo de interesse 
         
         
          IF (N_ELEMENTS(neighs) NE 1) THEN BEGIN ;confere se existem vizinhos de mesma classe guardados no vetor
            temp = [neighs[1:*] , list] ;guarda posição dos vizinhos e da posição livre
            temp = temp[0:N_ELEMENTS(temp)-2] ;extrai ultimos dois valores do vetor
            sortVec = temp[sort(temp)] ;posições ordenadas de forma crescente
            list = [sortVec[uniq(sortVec)] , -999L] ;tira posições repetidas
         ENDIF   
      ENDWHILE
      
      newIdReg++
      flag = WHERE(freePos EQ -1)   
   ENDREP UNTIL (flag[0] EQ -1)
   
   Return, Connected
   END