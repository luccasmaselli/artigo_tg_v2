FUNCTION create_command, ParamSTRUCT

eps = 0.001
Penalty = ParamSTRUCT.Penalty ;vindo do grid search...
Shrinking = 0
parametro = ParamSTRUCT.KernelParameters[1]

CASE ParamSTRUCT.Kernel OF
 0: command = './svm-train_LINUX -t 0' $ ;para ser Linear
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV' 
   
   1: command = './svm-train_LINUX -t 1' $ ;para ser Polinomial
          + ' -p ' + STRTRIM(STRING(parametro),1) $
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV' 
          
   2: command = './svm-train_LINUX -t 2' $ ;para ser RBF
          + ' -g ' + STRTRIM(STRING(parametro),1) $
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV'  
          
          
   3: command = './svm-train_LINUX -t 3' $ ;para ser TANH
          + ' -g ' + STRTRIM(STRING(parametro),1) $
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV'  
ENDCASE

return, command
          
END

;###########################################

FUNCTION new_create_command, ParamSTRUCT, indice

eps = 0.001
Penalty = ParamSTRUCT.Penalty[indice] ;vindo do grid search...
Shrinking = 0
parametro = ParamSTRUCT.KernelParameters[indice]

CASE ParamSTRUCT.Kernel[indice] OF
   0: command = './svm-train_LINUX -t 0' $ ;para ser Linear
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV' 
   
   1: command = './svm-train_LINUX -t 1' $ ;para ser Polinomial
          + ' -p ' + STRTRIM(STRING(parametro),1) $
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV' 
          
   2: command = './svm-train_LINUX -t 2' $ ;para ser RBF
          + ' -g ' + STRTRIM(STRING(parametro),1) $
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV'  
          
          
   3: command = './svm-train_LINUX -t 3' $ ;para ser TANH
          + ' -g ' + STRTRIM(STRING(parametro),1) $
          + ' -e ' + STRTRIM(STRING(eps),1) $
          + ' -c ' + STRTRIM(STRING(Penalty),1) $
          + ' -h ' + STRTRIM(STRING(Shrinking),1) $
          + ' -b 1 ' $ ;para estimação na forma de probabilidades...
          + ' TrainingLibSVM' + ' FileSV'  
          
ENDCASE

return, command
          
END