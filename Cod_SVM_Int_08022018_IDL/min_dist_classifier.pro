FUNCTION MIN_DIST_CLASSIFIER, win_stats, teus_stats
  
  dims_win = SIZE(win_stats, /dimension)
  NCol_win = dims_win[0]
  NLin_win = dims_win[1]
   
  dims_teu = SIZE(teus_stats, /dimension)
  NCol_teu = dims_teu[0]
  NLin_teu = dims_teu[1]
  NB_teu = dims_teu[2]
  
  norm_win_stats = DBLARR(NCol_win, NLin_win)
  ;norm_win_stats = DBLARR(NCol_win)
  norm_teus_stats = DBLARR(NCol_teu, NLin_teu, NB_teu)
  ;norm_teus_stats = DBLARR(NCol_teu, NLin_teu)
  medida = DBLARR(NLin_teu)
  
  FOR i=0L, NLin_win-1 DO BEGIN
    IF MAX(win_stats[*,i])-MIN(win_stats[*,i]) EQ 0 THEN DIV = 1 ELSE DIV = MAX(win_stats[*,i])-MIN(win_stats[*,i]) 
    norm_win_stats[*,i] = (win_stats[*,i]-MIN(win_stats[*,i]))/DIV
  ENDFOR
  ;IF MAX(win_stats)-MIN(win_stats) EQ 0 THEN DIV = 1 ELSE DIV = MAX(win_stats)-MIN(win_stats) 
   ; norm_win_stats = (win_stats-MIN(win_stats))/DIV
    
  FOR j=0L, NB_teu-1 DO BEGIN
    FOR k=0L, NLin_teu-1 DO BEGIN
      IF MAX(teus_stats[*,k,j])-MIN(teus_stats[*,k,j]) EQ 0 THEN DIV = 1 ELSE DIV = MAX(teus_stats[*,k,j])-MIN(teus_stats[*,k,j])
      norm_teus_stats[*,k,j] = (teus_stats[*,k,j]-MIN(teus_stats[*,k,j]))/DIV
    ENDFOR
  ENDFOR
;    FOR k=0L, NLin_teu-1 DO BEGIN
;      IF MAX(teus_stats[*,k])-MIN(teus_stats[*,k]) EQ 0 THEN DIV = 1 ELSE DIV = MAX(teus_stats[*,k])-MIN(teus_stats[*,k])
;      norm_teus_stats[*,k] = (teus_stats[*,k]-MIN(teus_stats[*,k]))/DIV
;    ENDFOR
;  
  
  FOR m=0L, NLin_teu-1 DO BEGIN
    FOR n=0L, NB_teu-1 DO BEGIN
      medida[m] = medida[m] + NORM(norm_win_stats[*,n] - norm_teus_stats[*,m,n])
    ENDFOR
;    cent_win = TOTAL(norm_win_stats,2)/N_ELEMENTS(norm_win_stats[0,*])
;    cent_teu = TOTAL(norm_teus_stats[*,m,*],3)/N_ELEMENTS(norm_teus_stats[0,*])
;    medida[m] = NORM(cent_win - cent_teu)
  ENDFOR
  
 Return, WHERE(medida EQ MIN(medida)) 
END