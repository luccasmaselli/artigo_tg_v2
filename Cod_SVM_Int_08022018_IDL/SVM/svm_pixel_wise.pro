FUNCTION SVM_PIXEL_WISE, PATH_IMG, PntROIs, TestROIs, Attributes, InterfaceParameters
;COMMON PkgPARAMETERS

;parametros gerais
ParameStruct = InterfaceParameters 

Image = READ_TIFF(PATH_IMG) ;lê a imagem tiff
Image = IMAGE_INSPECT(Image) ;Check image dimensions
Image = FLOAT(Image[Attributes,*,*]) ;Set the attribute

FOR i = 0, N_ELEMENTS(Image[*,0,0])-1 DO BEGIN
   IF MIN(Image[i,*,*]) LT 0 THEN $
      Image[i,*,*] = (Image[i,*,*] + ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] + ABS(MIN(Image[i,*,*]))) $
   ELSE $ 
      Image[i,*,*] = (Image[i,*,*] - ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] - ABS(MIN(Image[i,*,*])))
ENDFOR

Dims = size(Image,/dimension)   &   NC = Dims[1]   &   NL = Dims[2]

IMAGE2TEXT_LIBSVM_PREDICT_FORMAT, Image ;Imagem a ser usada no LibSVM (entrada em texto)

ROIs = RESAMPLE_ROI(PntROIs, 1.0) ;amostragem de sampling% do total de pixels

;Define the classes used in the binary separation
;The returned value is used to build the training samples (as ROIs)


BinDT = CLASS_SELECTOR(ROIs,ParameSTRUCT)
;>>> ATENCAO PARA ESTA FUNCAO "CLASS_SELECTOR"!
;Ela é responsável por organizar os dados de treinamento de acordo coma estratégia multiclasse
;Dê uma estudada nelas... OAO e OAA são as principais... mas existem umas variações que tentei verificar no passado...


DiImage = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
HipParams = PTRARR(N_ELEMENTS(BinDT[0,*]))

ti = systime(/second)
FOR i = 0, N_ELEMENTS(BinDT[0,*])-1 DO BEGIN
   R1 = *BinDT[0,i]  &  R1 = R1.RoiLex
   R2 = *BinDT[1,i]  &  R2 = R2.RoiLex

   ;Training data set building
   PtrTRAINING = BUILD_TRAINING_DATA(R1,R2,Image)
   
   
   ;>>>dentro desta função tem o comando 'command_train' (entre lá para ver e leia estas instruções abaixo...)
   ;você deve entender a sintaxe deste comando, que é passado como argumento para programa libsvm
   ;atualmente apenas o kernel rbf está implementado... (pois a opção '-g' está fixa e só deve ser incluida quando é rbf)
   ;seria extremamente interessante vc implementar uma função que recebe as informações de Param_STRUCT e dai...
   ;...dependendo do tipo do kernel, é escrita uma linha de comando válida
   ;veja como o libsvm funciona em: https://www.csie.ntu.edu.tw/~cjlin/libsvm/
   ;command = CREATE_COMMAND(ParameSTRUCT)   
   DiImage[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT__COMPLETE(PtrTRAINING, ParameSTRUCT)
ENDFOR

ClaImage = MULTICLASS_CLASSIFICATION(DiImage,ParameSTRUCT,PntROIs)
IndexImage = CLASSIF_INDEX(ClaImage,PntROIs)

ConfMatrix = CONFUSION_MATRIX(IndexImage, TestROIs)
Measures = CONCORDANCE_MEASURES(ConfMatrix)

;write_tiff, '/home/luccas/Desktop/Classificacao_.tif', ClaImage
Return, {ClassificationIndex: IndexImage, ColorClassification: ClaImage, $
         ConfusionMatrix:ConfMatrix, AccuracyMeasures: Measures}
  

;Return, [Measures[3], ParameStruct.Kernel, ParameStruct.KernelParameters[0], ParameStruct.Penalty[0]] 
END

;###############################################################

FUNCTION NEW_SVM_PIXEL_WISE, PATH_IMG, PntROIs, TestROIs, Attributes, InterfaceParameters, sampling
;COMMON PkgPARAMETERS

;parametros gerais
ParameStruct = InterfaceParameters 

Image = READ_TIFF(PATH_IMG) ;lê a imagem tiff
Image = IMAGE_INSPECT(Image) ;Check image dimensions
Image = FLOAT(Image[Attributes,*,*]) ;Set the attribute

FOR i = 0, N_ELEMENTS(Image[*,0,0])-1 DO BEGIN
   IF MIN(Image[i,*,*]) LT 0 THEN $
      Image[i,*,*] = (Image[i,*,*] + ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] + ABS(MIN(Image[i,*,*]))) $
   ELSE $ 
      Image[i,*,*] = (Image[i,*,*] - ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] - ABS(MIN(Image[i,*,*])))
ENDFOR

Dims = size(Image,/dimension)   &   NC = Dims[1]   &   NL = Dims[2]

IMAGE2TEXT_LIBSVM_PREDICT_FORMAT, Image ;Imagem a ser usada no LibSVM (entrada em texto)

ROIs = RESAMPLE_ROI(PntROIs, sampling) ;amostragem de sampling% do total de pixels

;Define the classes used in the binary separation
;The returned value is used to build the training samples (as ROIs)


BinDT = CLASS_SELECTOR(ROIs,ParameSTRUCT)
;>>> ATENCAO PARA ESTA FUNCAO "CLASS_SELECTOR"!
;Ela é responsável por organizar os dados de treinamento de acordo coma estratégia multiclasse
;Dê uma estudada nelas... OAO e OAA são as principais... mas existem umas variações que tentei verificar no passado...


DiImageLin = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
DiImagePol = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
DiImageRBF = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
DiImageSig = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
HipParams = PTRARR(N_ELEMENTS(BinDT[0,*]))

ti = systime(/second)

FOR i = 0, N_ELEMENTS(BinDT[0,*])-1 DO BEGIN
    R1 = *BinDT[0,i]  &  R1 = R1.RoiLex
    R2 = *BinDT[1,i]  &  R2 = R2.RoiLex

    ;Training data set building
    PtrTRAINING = BUILD_TRAINING_DATA(R1,R2,Image,indgen(N_elements(attributes)),InImg)
   
   
    ;>>>dentro desta função tem o comando 'command_train' (entre lá para ver e leia estas instruções abaixo...)
    ;você deve entender a sintaxe deste comando, que é passado como argumento para programa libsvm
    ;atualmente apenas o kernel rbf está implementado... (pois a opção '-g' está fixa e só deve ser incluida quando é rbf)
    ;seria extremamente interessante vc implementar uma função que recebe as informações de Param_STRUCT e dai...
    ;...dependendo do tipo do kernel, é escrita uma linha de comando válida
    ;veja como o libsvm funciona em: https://www.csie.ntu.edu.tw/~cjlin/libsvm/
   FOR l=0, N_ELEMENTS(ParameSTRUCT.Kernel)-1 DO BEGIN
    command = NEW_CREATE_COMMAND(ParameSTRUCT,l)   
      CASE l OF
        0: DiImageLin[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
        1: DiImagePol[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
        2: DiImageRBF[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
        3: DiImageSig[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
      ENDCASE
   ENDFOR
ENDFOR
;DESCOBRIR COMO DESEMPATAR O DiImage E SE AS OUTRAS VARIAVEIS (BINDT, etc)
;SÃO IGUAIS A DA ESTRATEGIA NAO HIBRIDA.

ClaImage = NEW_MULTICLASS_CLASSIFICATION(DiImageLin,DiImagePol,DiImageRBF,DiImageSig,ParameSTRUCT,PntROIs)
IndexImage = CLASSIF_INDEX(ClaImage,PntROIs)

ConfMatrix = CONFUSION_MATRIX(IndexImage, TestROIs)
Measures = CONCORDANCE_MEASURES(ConfMatrix)

Return, {ClassificationIndex: IndexImage, ColorClassification: ClaImage, $
         ConfusionMatrix:ConfMatrix, AccuracyMeasures: Measures}

END

;###############################################################################

FUNCTION NEW_SVM_PIXEL_WISE_LC, PATH_IMG, PntROIs, TestROIs, Attributes, InterfaceParameters, sampling, bestAlphas
;COMMON PkgPARAMETERS

;parametros gerais
ParameStruct = InterfaceParameters 

Image = READ_TIFF(PATH_IMG) ;lê a imagem tiff
Image = IMAGE_INSPECT(Image) ;Check image dimensions
Image = FLOAT(Image[Attributes,*,*]) ;Set the attribute

FOR i = 0, N_ELEMENTS(Image[*,0,0])-1 DO BEGIN
   IF MIN(Image[i,*,*]) LT 0 THEN $
      Image[i,*,*] = (Image[i,*,*] + ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] + ABS(MIN(Image[i,*,*]))) $
   ELSE $ 
      Image[i,*,*] = (Image[i,*,*] - ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] - ABS(MIN(Image[i,*,*])))
ENDFOR

Dims = size(Image,/dimension)   &   NC = Dims[1]   &   NL = Dims[2]

IMAGE2TEXT_LIBSVM_PREDICT_FORMAT, Image ;Imagem a ser usada no LibSVM (entrada em texto)

ROIs = RESAMPLE_ROI(PntROIs, sampling) ;amostragem de sampling% do total de pixels

;Define the classes used in the binary separation
;The returned value is used to build the training samples (as ROIs)


BinDT = CLASS_SELECTOR(ROIs,ParameSTRUCT)
;>>> ATENCAO PARA ESTA FUNCAO "CLASS_SELECTOR"!
;Ela é responsável por organizar os dados de treinamento de acordo coma estratégia multiclasse
;Dê uma estudada nelas... OAO e OAA são as principais... mas existem umas variações que tentei verificar no passado...


DiImageLin = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
DiImagePol = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
DiImageRBF = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
DiImageSig = FLTARR(N_ELEMENTS(BinDT[0,*]),NC,NL)
HipParams = PTRARR(N_ELEMENTS(BinDT[0,*]))

ti = systime(/second)

FOR i = 0, N_ELEMENTS(BinDT[0,*])-1 DO BEGIN
    R1 = *BinDT[0,i]  &  R1 = R1.RoiLex
    R2 = *BinDT[1,i]  &  R2 = R2.RoiLex

    ;Training data set building
    PtrTRAINING = BUILD_TRAINING_DATA(R1,R2,Image,indgen(N_elements(attributes)),InImg)
   
   
    ;>>>dentro desta função tem o comando 'command_train' (entre lá para ver e leia estas instruções abaixo...)
    ;você deve entender a sintaxe deste comando, que é passado como argumento para programa libsvm
    ;atualmente apenas o kernel rbf está implementado... (pois a opção '-g' está fixa e só deve ser incluida quando é rbf)
    ;seria extremamente interessante vc implementar uma função que recebe as informações de Param_STRUCT e dai...
    ;...dependendo do tipo do kernel, é escrita uma linha de comando válida
    ;veja como o libsvm funciona em: https://www.csie.ntu.edu.tw/~cjlin/libsvm/
    
   FOR l=0, N_ELEMENTS(ParameSTRUCT.Kernel)-1 DO BEGIN
    command = NEW_CREATE_COMMAND(ParameSTRUCT,l)   
      CASE l OF
        0: DiImageLin[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
        1: DiImagePol[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
        2: DiImageRBF[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
        3: DiImageSig[i,*,*] = INTERFACE_LIBSVM_TRAIN_PREDICT(PtrTRAINING, ParameSTRUCT, command)
      ENDCASE
   ENDFOR
ENDFOR
;DESCOBRIR COMO DESEMPATAR O DiImage E SE AS OUTRAS VARIAVEIS (BINDT, etc)
;SÃO IGUAIS A DA ESTRATEGIA NAO HIBRIDA.

;ClaImage = NEW_MULTICLASS_CLASSIFICATION(DiImageLin,DiImagePol,DiImageRBF,DiImageSig,ParameSTRUCT,PntROIs)
ClaImage = NEW_MULTICLASS_CLASSIFICATION_LC(DiImageLin,DiImagePol,DiImageRBF,DiImageSig,ParameSTRUCT,PntROIs,bestAlphas)

IndexImage = CLASSIF_INDEX(ClaImage,PntROIs)

ConfMatrix = CONFUSION_MATRIX(IndexImage, TestROIs)
Measures = CONCORDANCE_MEASURES(ConfMatrix)

Return, {ClassificationIndex: IndexImage, ColorClassification: ClaImage, $
         ConfusionMatrix:ConfMatrix, AccuracyMeasures: Measures}

END