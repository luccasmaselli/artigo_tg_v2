FUNCTION GROUP_CLASSES_BR, id_image
  
  dims = SIZE(id_image, /dimension)
  NCol = dims[0]
  NLin = dims[1]
  
  id_img_group = INTARR(NCol,NLin)
  
  INDEX = WHERE(id_image EQ 2, count)
  IF count NE 0 THEN id_img_group(INDEX) = 0 ;AGUA
  INDEX = WHERE(id_image EQ 1 OR id_image EQ 6, count)
  IF count NE 0 THEN id_img_group(INDEX) = 1 ;VEGETACAO
  INDEX = WHERE(id_image EQ 3 OR id_image EQ 4, count)
  IF count NE 0 THEN id_img_group(INDEX) = 2 ;ARRUAMENTO
  INDEX = WHERE(id_image EQ 0, count)
  IF count NE 0 THEN id_img_group(INDEX) = 3 ;EDIFICACOES CERAMICA
  INDEX = WHERE(id_image EQ 5, count)
  IF count NE 0 THEN id_img_group(INDEX) = 4 ;EDIFICACOES CLARO
  
  Return, id_img_group
  
  END