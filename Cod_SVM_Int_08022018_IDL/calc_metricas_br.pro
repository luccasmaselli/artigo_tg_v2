FUNCTION CALC_METRICAS_BR, image
    
  ;teu_comp_conect = INTARR(12,199,199)
  dims = SIZE(image, /dimension)
  ;NB = dims[0]
  NCol = dims[0]
  NLin = dims[1]
  ;Estats = LONARR(12,10,3) ;col (10 colunas => classes) & lin (3 linhas => 3 métricas)
  Estats = DBLARR(4,3) ;col (7 colunas => classes) & lin (3 linhas => métricas)
  
  ;cria vetores de dimensão igual ao número de classes
  sum_vec = FLTARR(4)
  count = FLTARR(4)
  media = FLTARR(4)
  dividendo_vec = FLTARR(4)
  
  
  ;FOR i=0L, N_ELEMENTS(ptrrois)-1 DO BEGIN
    ;Aux = *PtrRois[i]
   
   ;image = BUILD_IMAGE(img, Aux) ;muda a imagem de RGB para índices (0,1,2,...,9)

   comp_conect_img = CONNECTED_COMPONENTS(image) ;constrói a imagem de componentes conectados
   
   sort_vec = comp_conect_img[UNIQ(comp_conect_img, SORT(comp_conect_img))] ;ordena os índices de componentes conectados
   
   FOR j=0L, N_ELEMENTS(sort_vec)-1 DO BEGIN ;calculo das variáveis que compõem a média
    pos_id = WHERE(comp_conect_img eq j+1) ;acha todas as posições das componentes conectadas 
    sum_vec[image(pos_id(0))] = sum_vec[image(pos_id(0))] + N_ELEMENTS(pos_id) ;realiza a contagem de pixels de cada classe
    count[image(pos_id(0))] = count[image(pos_id(0))] + 1 ;conta o numero de manchas de cada classe
   ENDFOR
   
   ; elimina o problema da divisão 0/0
   idx = WHERE(count[*] EQ 0, aux)
   IF aux NE 0 THEN count(idx) = 1 

   media = sum_vec/count ;calcula o tamanho médio das manchas   
   
   FOR k=0L, N_ELEMENTS(sort_vec)-1 DO BEGIN ;calculo das variáveis que compõem o desvio padrão
    pos_id = WHERE(comp_conect_img eq k+1) ;acha todas as posições das componentes conectadas
    dividendo_vec[image(pos_id(0))] = dividendo_vec[image(pos_id(0))] + ((N_ELEMENTS(pos_id) - media(image(pos_id(0))))^2) ;calcula o vetor com o dividendo do desv pad
   ENDFOR
   
   desv_pad = SQRT(dividendo_vec/count)
   
   ; elimina o problema da divisão 0/0
   idx = WHERE(media[*] EQ 0, aux)
   IF aux NE 0 THEN media(idx) = 1 
   
   Coef_var = desv_pad/media
   
   Dens_manchas = count/(169.0) ;área da imagem
   
   Edge_lenghts = CONNECTED_COMPONENTS_EDGE(image)
   
   Dens_bordas = Edge_lenghts/(169.0)
   
  Estats(*,0) = Coef_var
  Estats(*,1) = Dens_manchas
  Estats(*,2) = Dens_bordas
  
  
  ;ENDFOR

  
  Return, Estats
END