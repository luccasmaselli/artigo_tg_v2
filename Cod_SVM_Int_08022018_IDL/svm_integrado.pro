@bocalib.pro

@compute_parameters.pro
@miscs.pro

@SVM_Functions.pro

@svm_pixel_wise.pro

@INTERFACE_LIBSVM_TRAIN_PREDICT.pro
@text2image_libsvm_predict_format.pro
@image2text_libsvm_predict_format.pro

@data_accuracy_svm.pro

@interface_libsvm_alpha.pro
@build_function_pw.pro
@interface_libsvm_grid_search.pro

@classification_report.pro

@auto_gs_svm_pw.pro

@grow.pro
@pack_roi.pro
@create_command.pro
@basic_svm.pto
@hibridsvm.pro
@newarchisvm.pro

PRO SVM_Integrado 
COMMON PkgOpSolvers, PATH_OP_SOLVERS

  basic_svm
  stop
  hibridsvm
  newarchisvm
  
  
   

stop


END