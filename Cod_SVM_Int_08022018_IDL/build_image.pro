FUNCTION BUILD_IMAGE, img, dims_win, posicoes, ptrrois_ik_class

    dims = SIZE(img, /dimension)
    NB = dims[0]
    NCol = dims[1]
    NLin = dims[2] 

    image = INTARR(NB,dims_win[0],dims_win[1])
    image_ids = INTARR(dims_win[0],dims_win[1])-1
    
    lin1 = posicoes[0]/NCol  &  lin2 = posicoes[N_ELEMENTS(posicoes)-1]/NCol
    col1 = posicoes[0] mod NCol  &  col2 = posicoes[N_ELEMENTS(posicoes)-1] mod NCol
    image = img[*,uint(col1):uint(col2),uint(lin1):uint(lin2)]
    
    FOR j=0L, N_ELEMENTS(ptrrois_ik_class)-1 DO BEGIN ;montar imagem de acordo com cada TEU
      Aux_ik_class = *ptrrois_ik_class[j]
      INDEX = WHERE(Aux_ik_class.RoiColor[0] EQ image[0,*,*] and Aux_ik_class.RoiColor[1] EQ image[1,*,*] and Aux_ik_class.RoiColor[2] EQ image[2,*,*],count)
      IF count NE 0 THEN image_ids(INDEX) = j
    ENDFOR
      
      ; 0 = AGUA; 1 = VEGETACAO; 2 = ARRUAMENTO e 3 = EDIFICAÇÕES
      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 0 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL_CERAMICA
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 255 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 1 ;VEGETACAO
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 0 and image[2,*,*] eq 255, count)
;      IF count NE 0 THEN image_ids(INDEX) = 0 ;AGUA
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 255 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 2 ;SOLO EXPOSTO
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 255 and image[2,*,*] eq 255, count) 
;      IF count NE 0 THEN image_ids(INDEX) = 0 ;PISCINA
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 0 and image[2,*,*] eq 255, count)
;      IF count NE 0 THEN image_ids(INDEX) = 2 ;ASFALTO
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 255 and image[2,*,*] eq 255, count) 
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;SOMBRA
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 0 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL CINZA
;      
;      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 165 and image[2,*,*] eq 0, count) 
;      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL CLARO
;      
;      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 100 and image[2,*,*] eq 0, count)
;      IF count NE 0 THEN image_ids(INDEX) = 1 ;PASTO
    
   Return, image_ids 
END