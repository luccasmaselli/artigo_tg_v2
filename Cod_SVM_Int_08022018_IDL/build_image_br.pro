FUNCTION BUILD_IMAGE_BR, image, ptrrois, classrois

    dims = size(image, /dimension)
    NB = dims[0]
    NCol = dims[1]
    NLin = dims[2] 

    ;image = INTARR(NB,NCol,NLin)
    image_ids = INTARR(NCol,NLin)
    
      ; 0 = AGUA; 1 = VEGETACAO; 2 = ARRUAMENTO e 3 = EDIFICAÇÕES
    
      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 0 and image[2,*,*] eq 0, count)
      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL_CERAMICA
      
      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 255 and image[2,*,*] eq 0, count)
      IF count NE 0 THEN image_ids(INDEX) = 1 ;VEGETACAO
      
      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 0 and image[2,*,*] eq 255, count)
      IF count NE 0 THEN image_ids(INDEX) = 0 ;AGUA
      
      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 255 and image[2,*,*] eq 0, count)
      IF count NE 0 THEN image_ids(INDEX) = 2 ;SOLO EXPOSTO
      
      INDEX = WHERE(image[0,*,*] eq 0 and image[1,*,*] eq 255 and image[2,*,*] eq 255, count) 
      IF count NE 0 THEN image_ids(INDEX) = 2 ;ASFALTO
      
      INDEX = WHERE(image[0,*,*] eq 255 and image[1,*,*] eq 0 and image[2,*,*] eq 255, count)
      IF count NE 0 THEN image_ids(INDEX) = 3 ;TEL_CINZA
      
      INDEX = WHERE(image[0,*,*] eq 176 and image[1,*,*] eq 48 and image[2,*,*] eq 96, count) 
      IF count NE 0 THEN image_ids(INDEX) = 1 ;PASTO
      
    
   Return, image_ids 
END