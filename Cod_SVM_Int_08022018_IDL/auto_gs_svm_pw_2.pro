FUNCTION AUTO_GS_SVM_PW_2, PATH_IMG, ptrROIs, Attributes, gamaPar, grauPol, gamaSig, penaltyPar, kFolds, trainPercent, sampling, Parameters_SVM, indice
COMMON PkgPARAMETERS
COMMON PkgPOINTERS, PtrTRAINING

;parametros gerais
;ParamSTRUCT = {Penalty: 0.0, $
;               Kernel: 2, $
 ;              KernelParameters: [0.0,1.0,0.0], $
  ;             Strategy: 0}
;-------------------------------

Image = READ_TIFF(PATH_IMG)
Image = IMAGE_INSPECT(Image) ;Check image dimensions
Image = FLOAT(Image[Attributes,*,*]) ;Set the attribute

FOR i = 0, N_ELEMENTS(Image[*,0,0])-1 DO BEGIN
   IF MIN(Image[i,*,*]) LT 0 THEN $
      Image[i,*,*] = (Image[i,*,*] + ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] + ABS(MIN(Image[i,*,*]))) $
   ELSE $ 
      Image[i,*,*] = (Image[i,*,*] - ABS(MIN(Image[i,*,*])))/MAX(Image[i,*,*] - ABS(MIN(Image[i,*,*])))
ENDFOR

IF Parameters_SVM.Kernel[indice] EQ 0 THEN BEGIN
loop = 1
parametro = 1
ENDIF
IF Parameters_SVM.Kernel[indice] EQ 1 THEN BEGIN
loop = N_ELEMENTS(grauPol) 
parametro = grauPol
ENDIF
IF Parameters_SVM.Kernel[indice] EQ 2 THEN BEGIN
loop = N_ELEMENTS(gamaPar)
parametro = gamaPar
ENDIF
IF Parameters_SVM.Kernel[indice] EQ 3 THEN BEGIN
loop = N_ELEMENTS(gamaSig)
parametro = gamaSig
ENDIF

;Matrix used to store the gridsearch information 
;gridMatrix = FLTARR(N_ELEMENTS(gamaPar), N_ELEMENTS(penaltyPar))
gridMatrix = FLTARR(loop, N_ELEMENTS(penaltyPar))


;fazer uma amosragem dentro de cada fold (para não pesar muito o svm)
ROIs = RESAMPLE_ROI(ptrROIs, sampling) ;amostragem aleatória de sampling% do total de pixels

;reformar... escolher regiões dentro dos ponteiros de cada classe
foldTV = GET_KFOLDS(Image, ROIs, kFolds, trainPercent) 


;FOR gam = 0, N_ELEMENTS(gamaPar)-1 DO BEGIN
FOR x = 0, loop-1 DO BEGIN
   FOR pen = 0, N_ELEMENTS(penaltyPar)-1 DO BEGIN

      ;parametros
      Parameters_SVM.Penalty = penaltyPar[pen]
      Parameters_SVM.KernelParameters[indice] = parametro[x]         
         
      accFold = FLTARR(kFolds)
          
      FOR folds = 0, kFolds-1 DO BEGIN
       
         PntROIs = *foldTV[0,folds]
         PntTEST = *foldTV[1,folds]
         
         ;Define the classes used in the binary separation
         ;The returned value is used to build the training samples (as ROIs)
         
         ;stop
         BinDT = CLASS_SELECTOR(PntROIs,Parameters_SVM)
         
         ;Sorteio para equilibrar as classes de treino -- evitar problema de desbalanceamento em OAA
         ;BinDT = CLASS_EQUALIZATION(BinDT)
         ;BinDT = CLASS_EQUALIZATION_SAMPLED(BinDT,0.3)         

         Dims = size(Image,/dimension)
         NC = Dims[1]
         NL = Dims[2]

         testData = DATA_ACCURACY_SVM(PntTEST, Image)
         Discrim = FLTARR(N_ELEMENTS(BinDT[0,*]), N_ELEMENTS(testData.labelClass))

         FOR i = 0, N_ELEMENTS(BinDT[0,*])-1 DO BEGIN
            R1 = *BinDT[0,i]  &  R1 = R1.RoiLex
            R2 = *BinDT[1,i]  &  R2 = R2.RoiLex            

            ;Training data set building
            PtrTRAINING = BUILD_TRAINING_DATA(R1,R2,Image,Attributes,InImg)
            command = NEW_CREATE_COMMAND(Parameters_SVM, indice)
            ;stop
            Discrim[i,*] = INTERFACE_LIBSVM_GRID_SEARCH(PtrTRAINING, Parameters_SVM,testData.dataClass, command)
            PTR_FREE, PtrTRAINING
         ENDFOR
         
        ; stop
         IF Parameters_SVM.Strategy EQ 0 THEN accFold[folds] = CHECK_OA_SVM_ACCURACY(Discrim,testData.labelClass)
         IF Parameters_SVM.Strategy EQ 1 THEN accFold[folds] = CHECK_OA_SVM_ACCURACY_OAO(Discrim,testData.labelClass,N_ELEMENTS(ptrROIs))   
         
         
         ;PTR_FREE, BinDT
         HEAP_GC   
      ENDFOR
            
      gridMatrix[x,pen] = MEAN(accFold)

   ENDFOR
   
ENDFOR

PTR_FREE, ROIs, foldTV
HEAP_GC ;coleta os lixos tambem...

;busca pela configuração de maior acuracia
FOR x = 0, loop-1 DO BEGIN
   FOR pen = 0, N_ELEMENTS(penaltyPar)-1 DO BEGIN
      IF gridMatrix[x,pen] EQ MAX(gridMatrix) THEN Return, [parametro[x],penaltyPar[pen]]
   ENDFOR
ENDFOR

stop
END




;######################################