;FUNCTION GROW, Img, Neigh
FUNCTION GROW, Neigh
COMMON PkgSeeds, Seeds
COMMON PkgIndex, IdReg, Idx
COMMON PkgDims, NC, NL
COMMON PkgSegm, Img    ;<<<<<<<<< Aliviar as chamadas recurssivas!

IF Neigh[0] EQ -1 THEN Return, Img ELSE BEGIN

   ;Idx = GET_ID_REGION(Img,Neigh[0])  ;ver os valores de regiao que estao em volta
   Idx = GET_ID_REGION(Neigh[0])  ;ver os valores de regiao que estao em volta

   ;é necessario criar uma nova regiao ...
   IF Idx EQ -1 THEN BEGIN
      IdReg++
      Idx = IdReg
   ENDIF 

   N = GET_NEIGHS(Neigh[0])

   ;Plantar a semente da vez (Neigh[0])
   Pos = Neigh[0]
   lin = FIX(Pos / NC)   &   col = FIX(Pos MOD NC)
   Img[col,lin] = Idx

   ;ver se pode acrescentar e talz... alguns ifs resolvem...
   IF N[0] NE -1 THEN Neigh = [Neigh , N]
   REMOVE_REPETITION, Neigh ;remove repeticaoes (adjacencia entre pixels)
   IF N_ELEMENTS(Neigh) EQ 1 THEN Neigh = [Neigh , -1] ;para identificar que é o ultimo e para na prox chamada recurssiva 
   REMOVE_SEED, Neigh[0] ;remover do vetor de sementes... (Seeds)
   Neigh = Neigh[1:N_ELEMENTS(Neigh)-1]  

   Img = GROW(Neigh) ;chamada recurssiva
ENDELSE

Return, Img
END




;############################################
FUNCTION GET_NEIGHS, Neigh
COMMON PkgDims, NC, NL
COMMON PkgSegm, Img

;OBS: Lexicographic position = lin*NC + col

Pos = Neigh[0]
lin = FIX(Pos / NC)   &   col = FIX(Pos MOD NC)
N = [0]

;esq sup
IF ((col - 1) GE 0)AND((lin -1) GE 0) THEN $
   IF (Img[col-1,lin-1] EQ 0) THEN N = [N, (lin-1)*NC + (col-1)]

;cima
IF ((lin -1) GE 0) THEN $
   IF (Img[col,lin-1] EQ 0) THEN N = [N, (lin-1)*NC + col]

;dir sup
IF ((col + 1) LT NC)AND((lin -1) GE 0) THEN $
   IF(Img[col+1,lin-1] EQ 0) THEN N = [N, (lin-1)*NC + (col+1)]   

;dir
IF ((col + 1) LT NC) THEN $
   IF (Img[col+1,lin] EQ 0) THEN N = [N, lin*NC + (col+1)]

;dir inf
IF ((col + 1) LT NC)AND((lin +1) LT NL) THEN $
   IF (Img[col+1,lin+1] EQ 0) THEN N = [N, (lin+1)*NC + (col+1)]

;baixo
IF ((lin +1) LT NL) THEN $
   IF (Img[col,lin+1] EQ 0) THEN N = [N, (lin+1)*NC + col]

;esq inf
IF ((col - 1) GE 0)AND((lin +1) LT NL) THEN $
   IF (Img[col-1,lin+1] EQ 0) THEN N = [N, (lin+1)*NC + (col-1)]
   
;esquerda
IF ((col - 1) GE 0) THEN $
   IF (Img[col-1,lin] EQ 0) THEN N = [N, lin*NC + (col-1)]

IF N_ELEMENTS(N) EQ 1 THEN Return, [-1] ELSE Return, N[1:N_ELEMENTS(N)-1]

END





;############################################
FUNCTION GET_ID_REGION, Pos
COMMON PkgDims, NC, NL
COMMON PkgSegm, Img

lin = LONG(Pos / NC)   &   col = LONG(Pos MOD NC)
R = [-1]

;esq sup
IF ((col - 1) GE 0)AND((lin -1) GE 0) THEN $
   IF (Img[col-1,lin-1] NE 0)AND(Img[col-1,lin-1] NE -9999) THEN R = [R, Img[col-1,lin-1]]

;cima
IF ((lin -1) GE 0) THEN $
   IF (Img[col,lin-1] NE 0)AND(Img[col,lin-1] NE -9999) THEN R = [R, Img[col,lin-1]]

;dir sup
IF ((col + 1) LT NC)AND((lin -1) GE 0) THEN $
   IF (Img[col+1,lin-1] NE 0)AND(Img[col+1,lin-1] NE -9999) THEN R = [R, Img[col+1,lin-1]]   

;dir
IF ((col + 1) LT NC) THEN $
   IF (Img[col+1,lin] NE 0)AND(Img[col+1,lin] NE -9999) THEN R = [R, Img[col+1,lin]]

;dir inf
IF ((col + 1) LT NC)AND((lin +1) LT NL) THEN $
   IF (Img[col+1,lin+1] NE 0)AND(Img[col+1,lin+1] NE -9999) THEN R = [R, Img[col+1,lin+1]]

;baixo
IF ((lin +1) LT NL) THEN $
   IF (Img[col,lin+1] NE 0)AND(Img[col,lin+1] NE -9999) THEN R = [R, Img[col,lin+1]]

;esq inf
IF ((col - 1) GE 0)AND((lin +1) LT NL) THEN $
   IF (Img[col-1,lin+1] NE 0)AND(Img[col-1,lin+1] NE -9999) THEN R = [R, Img[col-1,lin+1]]
   
;esquerda
IF ((col - 1) GE 0) THEN $
   IF (Img[col-1,lin] NE 0)AND(Img[col-1,lin] NE -9999) THEN R = [R, Img[col-1,lin]]


IF N_ELEMENTS(R) EQ 1 THEN Return, [-1] ELSE BEGIN
   R = R[1:N_ELEMENTS(R)-1]
   Return, MODAL_NEIGH(R)
ENDELSE

END


;###################################
FUNCTION MODAL_NEIGH, R

Aux = R[UNIQ(R[SORT(R)])] ;?????
Pos = Aux   &   Pos[*] = 0

FOR i = 0, N_ELEMENTS(Aux)-1 DO $
    Pos[i] = N_ELEMENTS(WHERE(R EQ Aux[i]))
    
Modal = WHERE(Pos EQ MAX(Pos))

IF N_ELEMENTS(Modal) EQ 1 THEN Return, Aux[Modal] $
ELSE Pos = LONG(RANDOMU(SYSTIME(/Seconds),1)*10000) MOD N_ELEMENTS(Modal)      

Return, Aux[Modal[Pos]]

END

;###################################
PRO REMOVE_REPETITION, Neigh

Aux = [-9999]

FOR i = 0, N_ELEMENTS(Neigh)-1 DO BEGIN
   
   c = 0
   FOR j = 0, N_ELEMENTS(Aux)-1 DO IF Neigh[i] EQ Aux[j] THEN c++
   
   IF c EQ 0 THEN Aux = [Aux,Neigh[i]]


ENDFOR

Neigh = Aux[1:N_ELEMENTS(Aux)-1]
END


;###################################
PRO REMOVE_SEED, N
COMMON PkgSeeds, Seeds

Pos = WHERE(Seeds EQ N)

IF Pos EQ 0 THEN Seeds = Seeds[1:N_ELEMENTS(Seeds)-1] ELSE $
   IF Pos EQ N_ELEMENTS(Seeds)-1 THEN Seeds = Seeds[0:N_ELEMENTS(Seeds)-2] ELSE $
      Seeds = [ Seeds[0:Pos-1] , Seeds[Pos+1:N_ELEMENTS(Seeds)-1] ]  

END

 