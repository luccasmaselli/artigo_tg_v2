@bocalib.pro

@compute_parameters.pro
@miscs.pro

@SVM_Functions.pro

@svm_pixel_wise.pro

;@INTERFACE_LIBSVM_TRAIN_PREDICT.pro
@text2image_libsvm_predict_format.pro
@image2text_libsvm_predict_format.pro

@data_accuracy_svm.pro

@interface_libsvm_alpha.pro
@build_function_pw.pro
@interface_libsvm_grid_search.pro

@classification_report.pro

@auto_gs_svm_pw.pro

@grow.pro
@pack_roi.pro
@create_command.pro

@interface_libsvm_train_predict__complete.pro


PRO SVM_TEU
COMMON PkgOpSolvers, PATH_OP_SOLVERS
  
  PATH_STATS_IMG = "C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO_TG\2 - RESULTADOS\2 - STATS_IMG\SENTINEL\h      37_img_estatisticas_conv_win_ls07_1999.tif"
  PATH_ROI_TREINO = "C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO_TG\1 - INSUMOS\ROIs\Sentinel\ROI_Sent_TEU_treino.txt"
;  PATH_ROI_TREINO = 'C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO INTERNACIONAL\1 - INSUMOS\ROIs\ROI_LS07_PRIM.txt'
  ;PATH_ROI_TREINO = '/home/luccas/Desktop/tentativa recomecar/ROIs_TEUs_recomeco_ls08_2017.txt'
  PATH_ROI_TESTE = "C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO_TG\1 - INSUMOS\ROIs\Sentinel\ROI_Sent_TEU_teste.txt"
;  PATH_ROI_TESTE = 'C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO INTERNACIONAL\1 - INSUMOS\ROIs\ROI_LS07_PRIM_teste.txt'
  ;PATH_ROI_TESTE = '/home/luccas/Desktop/tentativa recomecar/ROIs_TEUs_recomeco_teste_v2_ls08_2017.txt'
  PATH_RESULTS = "C:\Users\Luccas\Documents\Documents\UNESP\ARTIGO_TG\2 - RESULTADOS\3 - CLASS_TEU\Sentinel\h37\"
  
  stats_img = READ_TIFF(PATH_STATS_IMG)
;  PtrROItreino = ASCII_READ_ROI(PATH_ROI_TREINO)
;  PtrROIteste = ASCII_READ_ROI(PATH_ROI_TESTE)
  
  ;dims = SIZE(stats_img, /dimension)   ;<<<< fiz a troca
;  NCol = dims[0]
;  NLin = dims[1]
;  NB = dims[2]
  dims = GET_DIMENSIONS(stats_img)
  NCol = dims[1]
  NLin = dims[2]
  NB = dims[0]
  
  Attributes = INTARR(NB)
  
  EstMult = [0,1] & Nestmult = N_ELEMENTS(EstMult)
  grauPol = [2, 3, 4, 5] & Ngrau = N_ELEMENTS(grauPol)
  gamaPar = [0.05, 0.1, 0.25, 0.5, 1.0, 1,5, 2.0, 3.0] &  Ngama = N_ELEMENTS(gamaPar)
  penaltyPar = [1 , 10 , 100 , 1000, 10000] & Npenalty = N_ELEMENTS(penaltyPar)
  
  posTab = 0L  ;<<<<usar isso p controlar sua tabela de kappas/configracoes
  KappasList = DBLARR(4,Npenalty*(1+Ngama+Ngrau)*3)  ;<<<esse *3 é para fazer dos kernels tambem
  
  FOR o=0l, Nestmult-1 DO BEGIN
    FOR k=0l, 2 DO BEGIN ; laço para escolher o Kernel (0=Linear, 1=Polinomial, 2=RBF)
    
      ;PARAMETERS#################################
    
      ;mudanca aqui... voce queria fazer o vetor [0,1,2,...,27]? teria que ser Attributes[att] = att
      ;FOR att=0L, NB-1 DO Attributes = [att] ;atributos da imagem original a serem considerados na classificação (bandas)
      Attributes = INDGEN(NB)  ;<<<< assim resolve melhor, ok?
    
      Parameters_SVM = {KernelParameters: [0.0,0.0,0.0], Penalty: 0, Kernel: k, Strategy: o}
      ;sampling = 0.99 
          
      PATH_OP_SOLVERS = 'C:\Users\Luccas\Documents\Documents\UNESP\TG\4 - CODS_IDL\PROJETO_TG_FINAL - Copia - Copia\SVM_Int_08022018\OptSolver\svm_solver\'
    ;###########################################
    
    
      IF Parameters_SVM.Kernel EQ 0 THEN BEGIN
        loop = 1
        parametro = 1
      ENDIF
      IF Parameters_SVM.Kernel EQ 1 THEN BEGIN
        loop = Ngrau
        parametro = grauPol
      ENDIF
      IF Parameters_SVM.Kernel EQ 2 THEN BEGIN
        loop = Ngama
        parametro = gamaPar
      ENDIF
    
      FOR i=0L, loop-1 DO BEGIN
        FOR j=0L, Npenalty-1 DO BEGIN
        
          IF k EQ 0 THEN BEGIN
            parameters_SVM.KernelParameters[0] = parametro
          ENDIF ELSE BEGIN
            parameters_SVM.KernelParameters[0] = parametro[i]
          ENDELSE
        
          parameters_SVM.Penalty[0] = penaltyPar[j]
          lin = MIN(WHERE(KappasList[1,*] EQ 0))
        
          PtrROItreino = ASCII_READ_ROI(PATH_ROI_TREINO)
          PtrROIteste = ASCII_READ_ROI(PATH_ROI_TESTE)
          StructRes = SVM_PIXEL_WISE(PATH_STATS_IMG, PtrROItreino, PtrROIteste, Attributes, Parameters_SVM)
        
          ;
          ;
          ;KappasList[*,posTab] = [StructRes.AccuracyMeasures[3],parameters_SVM.Kernel, $
          ;     parameters_SVM.KernelParameters[0],parameters_SVM.Penalty[0]]
          ;posTab++
          ; stop
Classification_report, PATH_RESULTS+'Sent_Report_ClassTEU_' + string(Parameters_SVM.Kernel) + '_' + string(Parameters_SVM.KernelParameters[0])+ '_' + string(Parameters_SVM.Penalty) + '_' + string(Parameters_SVM.Strategy) + '.txt', StructRes.ConfusionMatrix, StructRes.AccuracyMeasures, PtrROItreino, PtrROIteste, Parameters_SVM
write_tiff, PATH_RESULTS + 'Sent_ClassTEU_' + string(Parameters_SVM.Kernel) + '_' + string(Parameters_SVM.KernelParameters[0]) + '_' + string(Parameters_SVM.Penalty) + '_' + string(Parameters_SVM.Strategy) + '.tif', StructRes.ColorClassification       
       
       HEAP_GC, /ptr
       ENDFOR    
    ENDFOR  
 ;stop   
 ENDFOR
 ENDFOR

;  BestPar = WHERE(KappasList[0,*] EQ MAX(KappasList[0,*]))
;  Parameters_SVM.Kernel = KappasList[1,BestPar]
;  Parameters_SVM.KernelParameters[0] = KappasList[2,BestPar]
;  Parameters_SVM.Penalty[0] = KappasList[3,BestPar]
;  BestClass = SVM_PIXEL_WISE(PATH_STATS_IMG, PtrROItreino, PtrROIteste, Attributes, Parameters_SVM)
  
;   Classification_report, PATH_RESULTS+'ls07_1999_H19_Report_SVM_' + string(Parameters_SVM.Kernel) + '_' + string(Parameters_SVM.KernelParameters[0])+ '_' + string(Parameters_SVM.Penalty) + '.txt', BestClass.ConfusionMatrix, BestClass.AccuracyMeasures, PtrROItreino, PtrROIteste, Parameters_SVM
;   write_tiff, PATH_RESULTS + 'ls07_1999_H19_Classificacao_' + string(Parameters_SVM.Kernel) + '_' + string(Parameters_SVM.KernelParameters[0]) + '_' + string(Parameters_SVM.Penalty) + '.tif', BestClass.ColorClassification
END