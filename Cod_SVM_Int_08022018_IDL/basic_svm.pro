@bocalib.pro

@compute_parameters.pro
@miscs.pro

@SVM_Functions.pro

@svm_pixel_wise.pro

@INTERFACE_LIBSVM_TRAIN_PREDICT.pro
@text2image_libsvm_predict_format.pro
@image2text_libsvm_predict_format.pro

@data_accuracy_svm.pro

@interface_libsvm_alpha.pro
@build_function_pw.pro
@interface_libsvm_grid_search.pro

@classification_report.pro

@auto_gs_svm_pw.pro

@grow.pro
@pack_roi.pro
@create_command.pro



PRO Basic_SVM
COMMON PkgOpSolvers, PATH_OP_SOLVERS

  FOR n=0, 2 DO BEGIN ;laço para escolher as imagens (0=ikonos; 1=palsar; 2=landsat)
    FOR m=0, 3 DO BEGIN ;laço para escolher o kernel (0=linear; 1=polinomial; 2=RBF; 3=tanh)
      FOR o=0, 1 DO BEGIN ;laço para escolher a estrategia multiclasse (0=OAA; 1=OAO)
  
          ;PARAMETERS#################################
          Attributes = [0,1,2] ;atributos da imagem original a serem considerados na classificação (bandas)
          Parameters_SVM = {KernelParameters: [0.0,0.0,0.0], Penalty: 0, Kernel: m, Strategy: o}
          sampling = 0.99 ;amostragem para treinar o SVM -- esta usando apenas 5% dos pixels das amostras de treino, já que o objeto de classificacao sao regioes, e os valores pixels dentro das regioes sao todos iguais (após a vetorizacao) 
          
          PATH_OP_SOLVERS = '/home/luccas/Desktop/Default/OptSolver/svm_solver/'
          ;PATH_OP_SOLVERS =  '/home/rogerio/Dropbox/Luccas.IC/SVM_cods_Rogerio/IC_SVM/OptSolver/svm_solver/'  ;>>> LibSVM
          ;############################################
          
          
          ;GRID SEARCH SPACE#################################
          grauPol = [2, 3, 5, 7]
          gamaPar = [0.25, 0.5, 0.75, 1.0, 1.25, 1,5, 1.75, 2.0]   ;>> só esta implementado para RBF... fazer os demais kernels
          gamaSig = [0.25, 0.5, 0.75, 1.0, 1.25, 1,5, 1.75, 2.0]
          penaltyPar = [1 , 10 , 100 , 1000, 10000]
          ;############################################
          
          IF n EQ 0 THEN BEGIN
            PATH_IMG = '/home/luccas/Desktop/IKONOS_TESE/Ikonos.tif'
            PATH_ROI = '/home/luccas/Desktop/IKONOS_TESE/Rois_Treino.txt'
            PATH_TEST = '/home/luccas/Desktop/IKONOS_TESE/Rois_Teste.txt'
            PATH_RESULTS = '/home/luccas/Desktop/SVM_Integrado_08022018/Resultados/'
          ENDIF
          
          IF n EQ 1 THEN BEGIN 
            PATH_IMG = '/home/luccas/Desktop/PALSAR_600x600/Palsar.tif'
            PATH_ROI = '/home/luccas/Desktop/PALSAR_600x600/ROIs_Palsar3.txt'
            PATH_TEST = '/home/luccas/Desktop/PALSAR_600x600/ROIs_Palsar_Teste2.txt'
            PATH_RESULTS = '/home/luccas/Desktop/SVM_Integrado_08022018/Resultados/'
          ENDIF
          
          IF n EQ 2 THEN BEGIN
            PATH_IMG = '/home/luccas/Desktop/TAPAJOS_LANDSAT5-TM/LANDSAT5-TM_Tapajos_2009.tif'
            PATH_ROI = '/home/luccas/Desktop/TAPAJOS_LANDSAT5-TM/Rois_Treino.txt'
            PATH_TEST = '/home/luccas/Desktop/TAPAJOS_LANDSAT5-TM/Rois_Teste.txt'
            PATH_RESULTS = '/home/luccas/Desktop/SVM_Integrado_08022018/Resultados/'
          ENDIF
          
          PtrROIs = ASCII_READ_ROI(PATH_ROI) ;transforma rois em formato que a biblioteca reconhece
          PtrTEST = ASCII_READ_ROI(PATH_TEST) ;transforma rois em formato que a biblioteca reconhece
          
          ;command = create_command(Parameters_SVM, grauPol, gamaPar, penaltyPar)
          ;Incluir rotina para seleção dos parametros >> GridSearch...
          ;>>> ARRUMAR O CODIGO ABAIXO... mais pra frente...
          ;############################################
             t0 = SYSTIME(/seconds)
             bestConf_SVM = AUTO_GS_SVM_PW(PATH_IMG, PtrROIs, Attributes, $
             gamaPar, grauPol, gamaSig, penaltyPar, 10, 70.0, sampling, Parameters_SVM)
             Parameters_SVM.KernelParameters[1] = bestConf_SVM[0]
             Parameters_SVM.Penalty = bestConf_SVM[1]
             timeGS_SVM = SYSTIME(/seconds) - t0
          ;############################################
          ;stop
          t0 = SYSTIME(/seconds)
          ResSVM_pixWise = SVM_PIXEL_WISE(PATH_IMG, PtrROIs, PtrTEST, Attributes, Parameters_SVM, sampling)
          time_SVM = SYSTIME(/seconds) - t0
          
          IF n EQ  0 THEN BEGIN
            imagem='Ikonos'
          ENDIF
          
          IF n EQ  1 THEN BEGIN
            imagem = 'Palsar'
          ENDIF
          
          IF n EQ  2 THEN BEGIN
            imagem = 'Landsat'
          ENDIF
          

          IF m EQ  0 THEN BEGIN
            KernelString ='Linear'
          ENDIF
          
          IF m EQ  1 THEN BEGIN
            KernelString = 'Polinomial'
          ENDIF
          
          IF m EQ  2 THEN BEGIN
            KernelString = 'RBF'
          ENDIF         
          
          IF m EQ 3 THEN BEGIN
            KernelString = 'TANH'
          ENDIF
          
          
          IF o EQ  0 THEN BEGIN
            MulticlasseString = 'OAA'
          ENDIF         
          
          IF o EQ 1 THEN BEGIN
            MulticlasseString = 'OAO'
          ENDIF
          
          bestAlphas = [0.0,0.0,0.0,0.0]
          
          CLASSIFICATION_REPORT, PATH_RESULTS+'Report_SVM_' + imagem + '_' + KernelString + '_' + MulticlasseString + '.txt', ResSVM_pixWise.ConfusionMatrix, ResSVM_pixWise.AccuracyMeasures, time_SVM, PtrROIs, PtrTEST, Parameters_SVM, bestConf_SVM, bestAlphas, timeGS_SVM
          
          print, "Gravando resultado classificação SVM..."
          write_tiff, PATH_RESULTS + 'Classificacao_' + imagem + '_' + KernelString + '_' + MulticlasseString + '.tif', ResSVM_pixWise.ColorClassification
          
          tvscl, ResSVM_pixWise.ColorClassification, true=1
          
      ENDFOR
    ENDFOR  
  ENDFOR
END