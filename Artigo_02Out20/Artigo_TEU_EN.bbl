\begin{thebibliography}{31}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\href}[2]{#2}
\providecommand{\path}[1]{#1}
\providecommand{\DOIprefix}{doi:}
\providecommand{\ArXivprefix}{arXiv:}
\providecommand{\URLprefix}{URL: }
\providecommand{\Pubmedprefix}{pmid:}
\providecommand{\doi}[1]{\href{http://dx.doi.org/#1}{\path{#1}}}
\providecommand{\Pubmed}[1]{\href{pmid:#1}{\path{#1}}}
\providecommand{\bibinfo}[2]{#2}
\ifx\xfnm\relax \def\xfnm[#1]{\unskip,\space#1}\fi
%Type = Article
\bibitem[{Aljoufie et~al.(2013)Aljoufie, Zuidgeest, Brussel and [van
  Maarseveen]}]{aljoufie2013}
\bibinfo{author}{Aljoufie, M.}, \bibinfo{author}{Zuidgeest, M.},
  \bibinfo{author}{Brussel, M.}, \bibinfo{author}{[van Maarseveen], M.},
  \bibinfo{year}{2013}.
\newblock \bibinfo{title}{Spatial–temporal analysis of urban growth and
  transportation in jeddah city, saudi arabia}.
\newblock \bibinfo{journal}{Cities} \bibinfo{volume}{31},
  \bibinfo{pages}{57--68}.
\newblock \DOIprefix\doi{10.1016/j.cities.2012.04.008}.
%Type = Article
\bibitem[{Banzhaf and Hofer(2008)}]{banzhaf2008}
\bibinfo{author}{Banzhaf, E.}, \bibinfo{author}{Hofer, R.},
  \bibinfo{year}{2008}.
\newblock \bibinfo{title}{Monitoring urban structure types as spatial
  indicators with {CIR} aerial photographs for a more effective urban
  environmental management}.
\newblock \bibinfo{journal}{IEEE Journal of Selected Topics in Applied Earth
  Observations and Remote Sensing} \bibinfo{volume}{1},
  \bibinfo{pages}{129--138}.
%Type = Article
\bibitem[{Banzhaf et~al.(2009)Banzhaf, H\"ofer and Romero}]{banzhaf2009}
\bibinfo{author}{Banzhaf, E.}, \bibinfo{author}{H\"ofer, R.},
  \bibinfo{author}{Romero, H.}, \bibinfo{year}{2009}.
\newblock \bibinfo{title}{Analysing dynamic parameters for urban heat stress
  incorporating the spatial distribution of urban structure types}.
\newblock \bibinfo{journal}{IEEE Urban Remote Sensing Joint Event} ,
  \bibinfo{pages}{1--4}.
%Type = Article
\bibitem[{Berger et~al.(2018)Berger, Voltersen, Schmullius and
  Hese}]{berger2018}
\bibinfo{author}{Berger, C.}, \bibinfo{author}{Voltersen, M.},
  \bibinfo{author}{Schmullius, C.}, \bibinfo{author}{Hese, S.},
  \bibinfo{year}{2018}.
\newblock \bibinfo{title}{Robust mapping of urban structure types using high
  resolution geospatial data}.
\newblock \bibinfo{journal}{gis.Science 2} , \bibinfo{pages}{47--59}.
%Type = Article
\bibitem[{B\"ohm(1998)}]{bohm1998}
\bibinfo{author}{B\"ohm, P.}, \bibinfo{year}{1998}.
\newblock \bibinfo{title}{Urban structural units as a key indicator for
  monitoring and optimizing the urban environment}.
\newblock \bibinfo{journal}{Urban Ecology} .
%Type = Article
\bibitem[{Bruzzone and Persello(2009)}]{bruzzone2009}
\bibinfo{author}{Bruzzone, L.}, \bibinfo{author}{Persello, C.},
  \bibinfo{year}{2009}.
\newblock \bibinfo{title}{A novel context-sensitive semisupervised svm
  classifier robust to mislabeled training samples}.
\newblock \bibinfo{journal}{IEEE Transactions on Geoscience and Remote Sensing}
  \bibinfo{volume}{47}, \bibinfo{pages}{2142--2154}.
%Type = Article
\bibitem[{Chavez and Kwarteng(1989)}]{chavez1989}
\bibinfo{author}{Chavez, P.S.}, \bibinfo{author}{Kwarteng, A.Y.},
  \bibinfo{year}{1989}.
\newblock \bibinfo{title}{Extracting spectral contrast in landsat thematic
  mapper image data using selective principal component analysis}.
\newblock \bibinfo{journal}{Photogrammetric Engineering Remote Sensing}
  \bibinfo{volume}{55}, \bibinfo{pages}{339--348}.
%Type = Book
\bibitem[{Congalton and Green(2009)}]{congalton2009}
\bibinfo{author}{Congalton, R.G.}, \bibinfo{author}{Green, K.},
  \bibinfo{year}{2009}.
\newblock \bibinfo{title}{Assessing the accuracy of remotely sensed data:
  \normalfont{principles and practices}}.
\newblock \bibinfo{edition}{2} ed., \bibinfo{publisher}{CRC Press/Taylor \&
  Francis}, \bibinfo{address}{Boca Raton}.
%Type = Article
\bibitem[{Deng et~al.(2009)Deng, Wand, Hong and Qi}]{deng2009}
\bibinfo{author}{Deng, J.S.}, \bibinfo{author}{Wand, K.},
  \bibinfo{author}{Hong, Y.}, \bibinfo{author}{Qi, J.G.}, \bibinfo{year}{2009}.
\newblock \bibinfo{title}{Spatio-temporal dynamics and evolution of land use
  change and landscape pattern in response to rapid urbanization}.
\newblock \bibinfo{journal}{Landscape Urban Plan.} \bibinfo{volume}{92},
  \bibinfo{pages}{187--198}.
%Type = Article
\bibitem[{Grizonnet et~al.(2017)Grizonnet, Michel, Poughon, Inglada, Savinaud
  and Cresson}]{grizonnet2017}
\bibinfo{author}{Grizonnet, M.}, \bibinfo{author}{Michel, J.},
  \bibinfo{author}{Poughon, V.}, \bibinfo{author}{Inglada, J.},
  \bibinfo{author}{Savinaud, M.}, \bibinfo{author}{Cresson, R.},
  \bibinfo{year}{2017}.
\newblock \bibinfo{title}{Orfeo toolbox: open source processing of remote
  sensing images}.
\newblock \bibinfo{journal}{Open geospatial data, software and standards}
  \bibinfo{volume}{2}.
%Type = Inproceedings
\bibitem[{Hecht et~al.(2013)Hecht, Herold, Meinel and Buchroithner}]{hecht2013}
\bibinfo{author}{Hecht, R.}, \bibinfo{author}{Herold, H.},
  \bibinfo{author}{Meinel, G.}, \bibinfo{author}{Buchroithner, M.},
  \bibinfo{year}{2013}.
\newblock \bibinfo{title}{Automatic derivation of urban structure types from
  topographic maps by means of image analysis and machine learning}, in:
  \bibinfo{booktitle}{26th International Cartographic Conference}.
%Type = Article
\bibitem[{Herold et~al.(2003)Herold, Goldstein and Clarke}]{herold2003}
\bibinfo{author}{Herold, M.}, \bibinfo{author}{Goldstein, N.C.},
  \bibinfo{author}{Clarke, K.C.}, \bibinfo{year}{2003}.
\newblock \bibinfo{title}{The spatiotemporal form of urban growth: measurement,
  analysis and modeling}.
\newblock \bibinfo{journal}{Remote Sensing of Environment}
  \bibinfo{volume}{86}, \bibinfo{pages}{286--302}.
%Type = Article
\bibitem[{Herold et~al.(2005)Herold, Hemphill, Dietzel and Clarke}]{herold2005}
\bibinfo{author}{Herold, M.}, \bibinfo{author}{Hemphill, J.},
  \bibinfo{author}{Dietzel, C.}, \bibinfo{author}{Clarke, K.C.},
  \bibinfo{year}{2005}.
\newblock \bibinfo{title}{Remote sensing derived mapping to support urban
  growth theory}.
\newblock \bibinfo{journal}{Joint Symposia URBAN - URS 2005. Remote Sensing and
  Urban Growth Theory} .
%Type = Article
\bibitem[{Herold et~al.(2002)Herold, Scepan and Clarke}]{herold2002b}
\bibinfo{author}{Herold, M.}, \bibinfo{author}{Scepan, J.},
  \bibinfo{author}{Clarke, K.C.}, \bibinfo{year}{2002}.
\newblock \bibinfo{title}{The use of remote sensing and landscape metrics to
  describe structures and changes in urban land uses}.
\newblock \bibinfo{journal}{Environment and Planning A: Economy and Space}
  \bibinfo{volume}{34}, \bibinfo{pages}{1443--1458}.
\newblock \DOIprefix\doi{10.1068/a3496}.
%Type = Article
\bibitem[{Huang et~al.(2015)Huang, Liu and Zhang}]{huang2015}
\bibinfo{author}{Huang, X.}, \bibinfo{author}{Liu, H.}, \bibinfo{author}{Zhang,
  L.}, \bibinfo{year}{2015}.
\newblock \bibinfo{title}{Spatiotemporal detection and analysis of urban
  villages in mega city regions of china using high-resolution remotely sensed
  imagery}.
\newblock \bibinfo{journal}{IEEE Transactions on Geoscience and Remote Sensing}
  \bibinfo{volume}{53}, \bibinfo{pages}{3639--3657}.
%Type = Article
\bibitem[{Lehner and Blaschke(2019)}]{LehnerBlaschke2019}
\bibinfo{author}{Lehner, A.}, \bibinfo{author}{Blaschke, T.},
  \bibinfo{year}{2019}.
\newblock \bibinfo{title}{A generic classification scheme for urban structure
  types}.
\newblock \bibinfo{journal}{Remote Sensing} \bibinfo{volume}{2},
  \bibinfo{pages}{1--11}.
\newblock \DOIprefix\doi{https://doi.org/10.3390/rs11020173}.
%Type = Article
\bibitem[{Lu and Weng(2007)}]{lu2007}
\bibinfo{author}{Lu, D.}, \bibinfo{author}{Weng, Q.}, \bibinfo{year}{2007}.
\newblock \bibinfo{title}{A survey of image classification methods and
  techniques for improving classification performance}.
\newblock \bibinfo{journal}{Int. J. Remote Sens.} \bibinfo{volume}{28},
  \bibinfo{pages}{823--870}.
\newblock \DOIprefix\doi{10.1080/01431160600746456}.
%Type = Book
\bibitem[{Mather(2004)}]{mather2004}
\bibinfo{author}{Mather, P.M.}, \bibinfo{year}{2004}.
\newblock \bibinfo{title}{Computer Processing of Remotely-Sensed Images: An
  Introduction}.
\newblock \bibinfo{publisher}{John Wiley \& Sons}.
%Type = Inproceedings
\bibitem[{{Montanges} et~al.(2015){Montanges}, {Moser}, {Taubenböck}, {Wurm}
  and {Tuia}}]{montanges2015}
\bibinfo{author}{{Montanges}, A.P.}, \bibinfo{author}{{Moser}, G.},
  \bibinfo{author}{{Taubenböck}, H.}, \bibinfo{author}{{Wurm}, M.},
  \bibinfo{author}{{Tuia}, D.}, \bibinfo{year}{2015}.
\newblock \bibinfo{title}{Classification of urban structural types with
  multisource data and structured models}, in: \bibinfo{booktitle}{2015 Joint
  Urban Remote Sensing Event (JURSE)}, pp. \bibinfo{pages}{1--4}.
\newblock \DOIprefix\doi{10.1109/JURSE.2015.7120489}.
%Type = Article
\bibitem[{Moon et~al.(2009)Moon, Downes, Rujner and Storch}]{moon2009}
\bibinfo{author}{Moon, K.}, \bibinfo{author}{Downes, N.},
  \bibinfo{author}{Rujner, H.}, \bibinfo{author}{Storch, H.},
  \bibinfo{year}{2009}.
\newblock \bibinfo{title}{Adaptation of the urban structure type approach for
  the assessment of climate change risks in ho chi minh city}.
\newblock \bibinfo{journal}{45 ISOCARP} , \bibinfo{pages}{1--7}.
%Type = Article
\bibitem[{Mountrakis et~al.(2011)Mountrakis, Im and Ogole}]{mountrakis2011}
\bibinfo{author}{Mountrakis, G.}, \bibinfo{author}{Im, J.},
  \bibinfo{author}{Ogole, C.}, \bibinfo{year}{2011}.
\newblock \bibinfo{title}{{S}upport {V}ector {M}achines in {R}emote {S}ensing:
  A review}.
\newblock \bibinfo{journal}{{ISPRS} Journal of Photogrammetry and Remote
  Sensing Society} \bibinfo{volume}{66}, \bibinfo{pages}{247--259}.
\newblock \DOIprefix\doi{10.1016/j.isprsjprs.2010.11.001}.
%Type = Article
\bibitem[{Novack and Stilla(2017)}]{novack2017}
\bibinfo{author}{Novack, T.}, \bibinfo{author}{Stilla, U.},
  \bibinfo{year}{2017}.
\newblock \bibinfo{title}{Context-based classification of urban blocks
  according to their built-up structure}.
\newblock \bibinfo{journal}{PFG -- Journal of Photogrammetry, Remote Sensing
  and Geoinformation Science} \bibinfo{volume}{85}, \bibinfo{pages}{365--376}.
\newblock \DOIprefix\doi{10.1007/s41064-017-0039-7}.
%Type = Article
\bibitem[{Pauleit and Duhme(2000)}]{pauleit2000}
\bibinfo{author}{Pauleit, S.}, \bibinfo{author}{Duhme, F.},
  \bibinfo{year}{2000}.
\newblock \bibinfo{title}{Assessing the environmental performance of land cover
  types for urban planning}.
\newblock \bibinfo{journal}{Landscape and Urban Planning} \bibinfo{volume}{52},
  \bibinfo{pages}{1--20}.
\newblock \DOIprefix\doi{10.1016/S0169-2046(00)00109-2}.
%Type = Article
\bibitem[{Pham et~al.(2011)Pham, Yamaguchi and Bui}]{pham2011}
\bibinfo{author}{Pham, H.M.}, \bibinfo{author}{Yamaguchi, Y.},
  \bibinfo{author}{Bui, T.Q.}, \bibinfo{year}{2011}.
\newblock \bibinfo{title}{A case study on the relation between city planning
  and urban growth using remote sensing and spatial metrics}.
\newblock \bibinfo{journal}{Landscape and Urban Planning} ,
  \bibinfo{pages}{223--230}.
%Type = Article
\bibitem[{Simanjuntak et~al.(2019)Simanjuntak, K. and
  Reckien}]{simanjuntak2019}
\bibinfo{author}{Simanjuntak, R.M.}, \bibinfo{author}{K., M.},
  \bibinfo{author}{Reckien, D.}, \bibinfo{year}{2019}.
\newblock \bibinfo{title}{Object-based image analysis to map local climate
  zones: The case of bandung, indonesia}.
\newblock \bibinfo{journal}{Applied Geography} \bibinfo{volume}{106},
  \bibinfo{pages}{108 -- 121}.
\newblock \DOIprefix\doi{10.1016/j.apgeog.2019.04.001}.
%Type = Article
\bibitem[{Stewart and Oke(2012)}]{stewart2012}
\bibinfo{author}{Stewart, I.D.}, \bibinfo{author}{Oke, T.R.},
  \bibinfo{year}{2012}.
\newblock \bibinfo{title}{{Local Climate Zones for Urban Temperature Studies}}.
\newblock \bibinfo{journal}{Bulletin of the American Meteorological Society}
  \bibinfo{volume}{93}, \bibinfo{pages}{1879--1900}.
\newblock \DOIprefix\doi{10.1175/BAMS-D-11-00019.1}.
%Type = Article
\bibitem[{Tam et~al.(2018)Tam, Abd~Rahman, Harun and Kaoje}]{tam2018}
\bibinfo{author}{Tam, T.H.}, \bibinfo{author}{Abd~Rahman, M.Z.},
  \bibinfo{author}{Harun, S.}, \bibinfo{author}{Kaoje, I.U.},
  \bibinfo{year}{2018}.
\newblock \bibinfo{title}{Mapping of highly heterogeneous urban structure type
  for flood vulnerability assessment}.
\newblock \bibinfo{journal}{ISPRS - International Archives of the
  Photogrammetry, Remote Sensing and Spatial Information Sciences}
  \bibinfo{volume}{XLII-4/W9}, \bibinfo{pages}{229--235}.
\newblock \DOIprefix\doi{10.5194/isprs-archives-XLII-4-W9-229-2018}.
%Type = Book
\bibitem[{Theodoridis and Koutroumbas(2008)}]{theodoridis2008}
\bibinfo{author}{Theodoridis, S.}, \bibinfo{author}{Koutroumbas, K.},
  \bibinfo{year}{2008}.
\newblock \bibinfo{title}{Pattern Recognition, Fourth Edition}.
\newblock \bibinfo{edition}{4th} ed., \bibinfo{publisher}{Academic Press,
  Inc.}, \bibinfo{address}{Orlando, FL, USA}.
%Type = Article
\bibitem[{Tomás et~al.(2016)Tomás, Fonseca, Almeida, Leonardi and
  Pereira}]{tomas2016}
\bibinfo{author}{Tomás, L.}, \bibinfo{author}{Fonseca, L.},
  \bibinfo{author}{Almeida, C.}, \bibinfo{author}{Leonardi, F.},
  \bibinfo{author}{Pereira, M.}, \bibinfo{year}{2016}.
\newblock \bibinfo{title}{Urban population estimation based on residential
  buildings volume using ikonos-2 images and lidar data}.
\newblock \bibinfo{journal}{International Journal of Remote Sensing}
  \bibinfo{volume}{37}, \bibinfo{pages}{1--28}.
\newblock \DOIprefix\doi{10.1080/01431161.2015.1121301}.
%Type = Book
\bibitem[{Webb and Copsey(2011)}]{webb2011}
\bibinfo{author}{Webb, A.R.}, \bibinfo{author}{Copsey, K.D.},
  \bibinfo{year}{2011}.
\newblock \bibinfo{title}{Statistical Pattern Recognition}.
\newblock \bibinfo{edition}{3} ed., \bibinfo{publisher}{Wiley}.
%Type = Article
\bibitem[{Wieland et~al.(2016)Wieland, Torres, Pittore and
  Benito}]{wieland2016}
\bibinfo{author}{Wieland, M.}, \bibinfo{author}{Torres, Y.},
  \bibinfo{author}{Pittore, M.}, \bibinfo{author}{Benito, B.},
  \bibinfo{year}{2016}.
\newblock \bibinfo{title}{Object-based urban structure type pattern recognition
  from landsat tm with a support vector machine}.
\newblock \bibinfo{journal}{International Journal of Remote Sensing}
  \bibinfo{volume}{37}, \bibinfo{pages}{4059--4083}.
\newblock \DOIprefix\doi{10.1080/01431161.2016.1207261}.

\end{thebibliography}
